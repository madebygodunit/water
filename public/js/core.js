//   /\_|_/\   [WATER]
//  /       \  samokat
const MAIN_CAMERA_BASIC_DIMENSION = 1.5;
const MAIN_CAMERA_ALBUM_FOV = 50;
let mainCameraFovFactor = 1;
let screenStat = 0;
const mainRenderer = new THREE.WebGLRenderer({ alpha: true, antialias: true });
mainRenderer.setPixelRatio(window.devicePixelRatio);
mainRenderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(mainRenderer.domElement);
mainRenderer.domElement.style.position = 'absolute';
mainRenderer.domElement.style.top = 0;

window.isMobile = function() {
  let check = false;
  (function(a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true })(navigator.userAgent || navigator.vendor || window.opera);
  return check;
}
let sound;
function goSound() {
  sound = new Howl({
    src: ['sound/WaterMusic.mp3'],
    html5: true,
    loop: true,
    volume: 0.5
  });
  sound.play();
  if (!ios) {
    showSoundButton();
  }
}
let ios = true;
let goBlur = false;
const mainScene = new THREE.Scene();
const frontScene = new THREE.Scene();
frontScene.scale.set(1 / 12, 1, 1 / 12);
const mainCamera = new THREE.PerspectiveCamera(MAIN_CAMERA_ALBUM_FOV, window.innerWidth / window.innerHeight, 1, 21);
mainCamera.rotation.x = -Math.PI / 2;
const waterCenterContainer = new THREE.Object3D();
waterCenterContainer.rotation.x = -Math.PI / 2;
waterCenterContainer.position.y = -12;
const startScreenContainer = new THREE.Object3D();
const finScreenContainer = new THREE.Object3D();
const productContainer = new THREE.Object3D();
finScreenContainer.add(productContainer);
waterCenterContainer.add(startScreenContainer);
const frontCenterContainer = new THREE.Object3D();
frontCenterContainer.rotation.x = -Math.PI / 2
frontCenterContainer.position.y = -12;
const frontStartScreenContainer = new THREE.Object3D();
frontCenterContainer.add(frontStartScreenContainer, finScreenContainer);
const waterBottomContainer = new THREE.Object3D();
waterBottomContainer.rotation.x = -Math.PI / 2;
waterBottomContainer.position.y = -12;
mainScene.add(waterCenterContainer, waterBottomContainer);
const frontTopContainer = new THREE.Object3D();
frontTopContainer.rotation.x = -Math.PI / 2;
frontTopContainer.position.y = -12;
const frontBottomContainer = new THREE.Object3D();
frontBottomContainer.rotation.x = -Math.PI / 2;
frontBottomContainer.position.y = -12;
frontScene.add(frontTopContainer, frontCenterContainer, frontBottomContainer);
const waterGeometry = new THREE.PlaneBufferGeometry(150, 400);
water = new THREE.Water(waterGeometry, {
  color: 0xffffff,
  scale: 1,
  flowDirection: new THREE.Vector2(1, 1),
  textureWidth: 1024,
  textureHeight: 1024
});
water.rotation.x = -Math.PI / 2;
water.position.y = 1;
mainScene.add(water);
let loadingCount = 1;
let EuclidCircularARegular;
new THREE.FontLoader().load('fonts/EuclidCircularARegular.json', function(font) {
  EuclidCircularARegular = font;
  loadingCount ? loadingCount-- : goLoad();
});
let path = [];
loadSVG(`svg/svg0.svg`).then(data => {
  path[0] = data.paths;
  loadingCount ? loadingCount-- : goLoad();
});
function loadSVG(url) {
  return new Promise(resolve => {
    new THREE.SVGLoader().load(url, resolve)
  })
}
const text = {};
const svgPic = [];
let blurTween;
let EuclidCircularAMedium;
const loadingFull = 11;
const screenBlur = { value: 0 };
const pic = [];
function goLoad() {
  loadingCount = loadingFull;
  text.loading = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(`Загрузка...`, 0.4), 3), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 1 }));
  text.loading.geometry.computeBoundingBox();
  text.loading.position.set(-text.loading.geometry.boundingBox.max.x / 2, 2.2, 0);
  waterBottomContainer.add(text.loading);
  animateLoadingText(0);
  createSVG(0, 12, 0x000000);
  svgPic[0].scale.set(0.02, -0.02, 1);
  svgPic[0].position.set(-1.45, -0.5, 0);
  svgPic[0].plane = new THREE.Mesh(new THREE.PlaneBufferGeometry(150, 30), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.0000001 }));
  svgPic[0].plane.position.set(67, 10, 0);
  svgPic[0].add(svgPic[0].plane);
  frontTopContainer.add(svgPic[0]);
  gsap.to(svgPic[0].material, { duration: 0.3, opacity: 1, ease: "none" });
  gsap.from(svgPic[0].position, { duration: 0.5, y: svgPic[0].position.y - 0.3, ease: "power1.out", onComplete: function() {
    new THREE.FontLoader().load('fonts/EuclidCircularAMedium.json', function(font) {
      EuclidCircularAMedium = font;
      checkLoading();
    });
    loadSVG(`svg/svg1.svg`).then(data => {
      path[1] = data.paths;
      checkLoading();
    });
    loadSVG(`svg/svg2.svg`).then(data => {
      path[2] = data.paths;
      checkLoading();
    });
    loadSVG(`svg/svg3.svg`).then(data => {
      path[3] = data.paths;
      checkLoading();
    });
    for (let i = 0; i < 7; i++) {
      loadPIC(`textures/pic${i}.png`).then(texture => {
        pic[i] = new THREE.MeshBasicMaterial({ map: texture, transparent: true });
        checkLoading();
      });
    }
  } });
}
function loadPIC(url) {
  return new Promise(resolve => {
    new THREE.TextureLoader().load(url, resolve)
  })
}
const loadingTimer = { step: 0 };
function checkLoading() {
  loadingCount--;
  gsap.to(loadingTimer, { duration: 0.2, step: Math.round(100 / loadingFull * (loadingFull - loadingCount)), ease: "none", onUpdate: function() {
    document.getElementById('gradient').style.background = `linear-gradient(rgba(255, 255, 255, 0.95) ${100 - loadingTimer.step}%, rgba(48, 174, 191, 0.8))`;
  } });
  if (!loadingCount) {
    createToyGraphics();
  }
}
function animateLoadingText(step) {
  text.loading.geometry.dispose();
  text.loading.geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(`Загрузкa...`.slice(0, 8 + step), 0.4), 3);
  text.loading.geometry.NeedUpdate = true;
  setTimeout(function() {
    if (text.loading.material.opacity > 0) {
      step ++;
      if (step == 4) step = 0;
      animateLoadingText(step);
    }
  }, 200);
}
function createSVG(chosen, details, color) {
  svgPic[chosen] = new THREE.Object3D();
  svgPic[chosen].material = new THREE.MeshBasicMaterial({ color: color, transparent: true, opacity: 0 });
  for (let i = 0; i < path[chosen].length; i++) {
    const newPath = path[chosen][i];
    const shapes = newPath.toShapes(true);
    for (let j = 0; j < shapes.length; j++) {
      const mesh = new THREE.Mesh(new THREE.ShapeBufferGeometry(shapes[j], details), svgPic[chosen].material);
      svgPic[chosen].add(mesh);
    };
  }
}
let currentIntroScreen = 0;
let graphicsReady = false;
let gradientColor = [
  { r: 48, g: 174, b: 191, rNext: 20.3, gNext: 0.3, bNext: -15.4 },
  { r: 251, g: 177, b: 37, rNext: 0, gNext: -6.5, bNext: 2.9 },
  { r: 251, g: 112, b: 66, rNext: -10.4, gNext: 9.9, bNext: -1.6  },
  { r: 147, g: 211, b: 50, rNext: -9.9, gNext: -3.7, bNext: 14.1  },
  { r: 48, g: 174, b: 191 },
];
const introInterval = 0.8;
const finInterval = [0.7, 0.73];
const userInput = new THREE.Object3D();
const keyboard = new THREE.Object3D();
const BLACK_MATERIAL = new THREE.MeshBasicMaterial({ color: 0x000000 });
const keyboardData = [`1`, `2`, `3`, `4`, `5`, `6`, `7`, `8`, `9`, `0`,
`q`, `w`, `e`, `r`, `t`, `y`, `u`, `i`, `o`, `p`, `a`, `s`, `d`, `f`, `g`, `h`, `j`, `k`, `l`, `z`, `x`, `c`, `v`, `b`, `n`, `m`,
`+`, `-`, `!`, `?`, `,`, `.`,
` `,
`й`, `ц`, `у`, `к`, `е`, `н`, `г`, `ш`, `щ`, `з`, `х`, `ф`, `ы`, `в`, `а`, `п`, `р`, `о`, `л`, `д`, `ж`, `э`, `я`, `ч`, `с`, `м`, `и`, `т`, `ь`, `б`, `ю`, `ё`, `ъ`];
const button = {};
let isIos = true;
const popUp = new THREE.Object3D();
const icon = {};
function createToyGraphics() {
  icon.scroll = new THREE.Object3D();
  icon.scroll.material = new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 0 });
  icon.scroll.part = [];
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0.15, 0.5, 0, Math.PI);
  shape.absarc(0, -0.15, 0.5, Math.PI, Math.PI * 2);
  shape.absarc(0, -0.15, 0.45, Math.PI * 2, Math.PI, true);
  shape.absarc(0, 0.15, 0.45, Math.PI, 0, true);
  shape.lineTo(0.45, -0.15);
  shape.lineTo(0.5, -0.15);
  icon.scroll.part[0] = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 12), icon.scroll.material);
  icon.scroll.part[0].geometry.scale(0.75, 0.75, 1)
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0.15, 0.025, 0, Math.PI);
  shape.absarc(0, -0.2, 0.025, Math.PI, Math.PI * 2);
  icon.scroll.part[1] = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 3), icon.scroll.material);
  icon.scroll.part[1].geometry.scale(0.75, 0.75, 1)
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, -0.3, 0.1, Math.PI * 1.25, Math.PI * 1.75);
  shape.absarc(0.25, -0.15, 0.025, Math.PI * 1.75, Math.PI * 2.75);
  shape.absarc(0, -0.3, 0.05, Math.PI * 1.75, Math.PI * 1.25, true);
  shape.absarc(-0.25, -0.15, 0.025, Math.PI * 0.25, Math.PI * 1.25);
  icon.scroll.part[2] = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 3), icon.scroll.material);
  icon.scroll.part[2].geometry.scale(0.75, 0.75, 1)
  gsap.to([icon.scroll.part[1].position, icon.scroll.part[2].position], { duration: 1, y: 0.1, ease: "power1.inOut", repeat: -1, yoyo: true });
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, -0.15, 0.45, Math.PI * 2, Math.PI, true);
  shape.absarc(0, 0.15, 0.45, Math.PI, 0, true);
  icon.scroll.part[3] = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 12), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 0.000001 }));
  icon.scroll.add(icon.scroll.part[3], icon.scroll.part[0], icon.scroll.part[1], icon.scroll.part[2]);
  icon.scroll.scale = 0;
  icon.scroll.position.y = 1.5;
  frontBottomContainer.add(icon.scroll);
  popUp.plane = new THREE.Mesh(new THREE.PlaneBufferGeometry(100, 100), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0 }));
  popUp.text_1 = { object: new THREE.Object3D(), text: [] };
  popUp.text_2 = { object: new THREE.Object3D(), text: [] };
  createSeparatedText(popUp.text_1, 24, 0.336, 0.52, EuclidCircularAMedium, 0xffffff);
  createSeparatedText(popUp.text_2, 25, 0.224, 0.4, EuclidCircularAMedium, 0xffffff);
  popUp.text_1.object.position.set(0, 0.4, 0.01);
  popUp.text_2.object.position.set(0, -0.4, 0.01);
  createSVG(1, 12, 0xffffff);
  svgPic[1].scale.set(0.023, -0.023, 1);
  svgPic[1].position.set(-0.4, 1.7, 0.01);
  createSVG(2, 25, 0xffffff);
  svgPic[2].scale.set(0.023, -0.023, 1);
  svgPic[2].position.set(-3.2, 5.2, 0.01);
  button.sound = new THREE.Object3D();
  button.sound.plane = new THREE.Mesh(new THREE.PlaneBufferGeometry(1, 1), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.0000001 }));
  button.sound.pic = new THREE.Mesh(new THREE.PlaneBufferGeometry(0.6, 0.6), pic[6]);
  pic[6].color.setHex(0x000000);
  pic[6].opacity = 0;
  button.sound.add(button.sound.plane, button.sound.pic);
  frontTopContainer.add(button.sound);
  button.speed = new THREE.Object3D();
  button.speed.plane = new THREE.Mesh(new THREE.PlaneBufferGeometry(1, 1), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.0000001 }));
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0.2, 0.02, Math.PI, Math.PI / 3, true);
  shape.absarc(0.28, 0, 0.02, Math.PI / 3 * 7, Math.PI / 3 * 5, true);
  shape.absarc(0, -0.2, 0.02, Math.PI / 3 * 5, Math.PI, true);
  shape.absarc(0, -0.2, 0.07, Math.PI, Math.PI / 3 * 5);
  shape.absarc(0.28, 0, 0.07, Math.PI / 3 * 5, Math.PI / 3 * 7);
  shape.absarc(0, 0.2, 0.07, Math.PI / 3, Math.PI);
  shape.lineTo(-0.07, -0.2);
  shape.lineTo(-0.02, -0.2);
  shape.lineTo(-0.02, 0.2);
  button.speed.icon_1 = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 3), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 0 }));
  button.speed.icon_2 = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 3), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 0 }));
  button.speed.icon_1.position.x = -0.36;
  button.speed.icon_2.position.x = 0.1;
  button.speed.add(button.speed.plane, button.speed.icon_1, button.speed.icon_2);
  button.speed.position.y = 1.5;
  button.speed.scale.set(0, 0, 1);
  frontBottomContainer.add(button.speed);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-3.2, 0.5, 0.12, Math.PI * 0.5, Math.PI);
  shape.absarc(-3.2, -0.5, 0.12, Math.PI, Math.PI * 1.5);
  shape.absarc(3.2, -0.5, 0.12, Math.PI * 1.5, Math.PI * 2);
  shape.absarc(3.2, 0.5, 0.12, 0, Math.PI * 0.5);
  shape.lineTo(3.2, 0.59);
  shape.absarc(3.2, 0.5, 0.09, Math.PI * 0.5, 0, true);
  shape.absarc(3.2, -0.5, 0.09, Math.PI * 2, Math.PI * 1.5, true);
  shape.absarc(-3.2, -0.5, 0.09, Math.PI * 1.5, Math.PI, true);
  shape.absarc(-3.2, 0.5, 0.09, Math.PI, Math.PI * 0.5, true);
  shape.lineTo(3.2, 0.59);
  shape.lineTo(3.2, 0.62);
  
  button.product_3 = new THREE.Object3D();
  
  button.product_3.border = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 3), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 1 }));

  
  
  button.popUp = new THREE.Object3D();
  
  button.popUp.border = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 3), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 0 }));
  
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(3.2, 0.5, 0.09, Math.PI * 0.5, 0, true);
  shape.absarc(3.2, -0.5, 0.09, Math.PI * 2, Math.PI * 1.5, true);
  shape.absarc(-3.2, -0.5, 0.09, Math.PI * 1.5, Math.PI, true);
  shape.absarc(-3.2, 0.5, 0.09, Math.PI, Math.PI * 0.5, true);
  
  button.popUp.plane = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 3), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 0.0000001 }));
  button.popUp.plane.visible = false;
  button.popUp.text = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(`OK`, 0.23), 3), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 0 }));
  button.popUp.ready = false;
  button.popUp.text.geometry.center();
  
  button.popUp.text.position.z = 0;
  button.popUp.position.y = 1.5;
  
  
  button.product_3.plane = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 3), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.0000001 }));
  button.product_3.plane.visible = false;
  button.product_3.text = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(`Выбрать средство`, 0.224), 3), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 1 }));
  button.product_3.ready = false;
  button.product_3.text.geometry.center();
  
  button.product_3.text.position.z = 0;
  //button.product_3.position.y = 1.5;
  
  
  
  button.product_3.add(button.product_3.border, button.product_3.plane, button.product_3.text);
  
  button.product_3.position.y = -14.5;
  
  
  button.popUp.add(button.popUp.border, button.popUp.plane, button.popUp.text);
  
  frontBottomContainer.add(button.popUp);
  
  popUp.add(svgPic[2], svgPic[1], popUp.plane, popUp.text_1.object, popUp.text_2.object);
  waterCenterContainer.add(popUp);
  
  button.product_1 = new THREE.Object3D();
  button.product_1.plane = new THREE.Mesh(new THREE.PlaneBufferGeometry(5, 1), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.0000001 }));
  button.product_1.text = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(`ВЫБРАТЬ СРЕДСТВО`, 0.21), 3), new THREE.MeshBasicMaterial({ color: 0x595959, transparent: true, opacity: 1 }));
  button.product_1.text.geometry.center();
  //button.product_1.position.y = -19.9;
  button.product_1.add(button.product_1.plane, button.product_1.text);
  finScreenContainer.add(button.product_1);
  
  
  
  button.product_1.posY = [-19.9, -11.7];
  button.product_1.screenScale = [1, 0.65];
  
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-2.2, 0.5, 0.12, Math.PI * 0.5, Math.PI);
  shape.absarc(-2.2, -0.5, 0.12, Math.PI, Math.PI * 1.5);
  shape.absarc(2.2, -0.5, 0.12, Math.PI * 1.5, Math.PI * 2);
  shape.absarc(2.2, 0.5, 0.12, 0, Math.PI * 0.5);
  shape.lineTo(2.2, 0.59);
  shape.absarc(2.2, 0.5, 0.09, Math.PI * 0.5, 0, true);
  shape.absarc(2.2, -0.5, 0.09, Math.PI * 2, Math.PI * 1.5, true);
  shape.absarc(-2.2, -0.5, 0.09, Math.PI * 1.5, Math.PI, true);
  shape.absarc(-2.2, 0.5, 0.09, Math.PI, Math.PI * 0.5, true);
  shape.lineTo(2.2, 0.59);
  shape.lineTo(2.2, 0.62);

  
  
  button.product_2 = new THREE.Object3D();
  
  button.product_2.border = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 3), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 1 }));
  
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(2.5, 0.5, 0.09, Math.PI * 0.5, 0, true);
  shape.absarc(2.5, -0.5, 0.09, Math.PI * 2, Math.PI * 1.5, true);
  shape.absarc(-2.5, -0.5, 0.09, Math.PI * 1.5, Math.PI, true);
  shape.absarc(-2.5, 0.5, 0.09, Math.PI, Math.PI * 0.5, true);
  
  button.product_2.plane = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 3), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.0000001 }));
  button.product_2.plane.visible = false;
  button.product_2.text = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(`Выбрать средство`, 0.224), 3), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 1 }));
  button.product_2.ready = false;
  button.product_2.text.geometry.center();
  
  button.product_2.text.position.z = 0;
 // button.product_2.position.y = 1.5;
  
  
  
  button.product_2.add(button.product_2.border, button.product_2.plane, button.product_2.text);
  
  button.product_2.position.y = -23.4;
  
  finScreenContainer.add();
  
  

  text.footer = [];
  text.footer[0] = new THREE.Object3D();
  text.footer[0].line = [];
  for (let i = 0; i < 10; i++) {
    text.footer[0].line[i] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(waterText[26 + i], 0.15), 3), new THREE.MeshBasicMaterial({ color: 0xffffff }));
    text.footer[0].line[i].position.y = -0.28 * i;
    text.footer[0].add(text.footer[0].line[i]);
    
  }
  text.footer[0].plane = new THREE.Mesh(new THREE.PlaneBufferGeometry(1.75, 0.35), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.0000001 }));
  text.footer[0].plane.position.set(2.08, -0.78, 0);
  text.footer[0].underline = new THREE.Mesh(new THREE.PlaneBufferGeometry(1.7, 0.01), new THREE.MeshBasicMaterial({ color: 0xffffff }));
  text.footer[0].underline.position.set(2.08, -0.9, 0);

  text.footer[0].add(text.footer[0].plane, text.footer[0].underline);
  text.footer[0].position.set(-3.3, -26.8, 0);
  
  
  text.footer[1] = new THREE.Object3D();
  text.footer[1].line = [];
  for (let i = 0; i < 3; i++) {
    text.footer[1].line[i] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(waterText[36 + i], 0.10), 3), new THREE.MeshBasicMaterial({ color: 0xffffff }));
    text.footer[1].line[i].position.y = -0.28 * i;
    text.footer[1].add(text.footer[1].line[i]);
  
  }
  text.footer[1].plane = new THREE.Mesh(new THREE.PlaneBufferGeometry(1.15, 0.16), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.0000001 }));
  text.footer[1].plane.position.set(13.9, 0.02, 0);
  text.footer[1].underline = new THREE.Mesh(new THREE.PlaneBufferGeometry(1.1, 0.007), new THREE.MeshBasicMaterial({ color: 0xffffff }));
  text.footer[1].underline.position.set(13.9, -0.03, 0);
  text.footer[1].add(text.footer[1].plane, text.footer[1].underline);
  text.footer[1].line[3] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(`ООО «Умный ритейл» © 2022`, 0.10), 3), new THREE.MeshBasicMaterial({ color: 0xffffff }));

  
  text.footer[1].position.set(-7.2, -18, 0);
  
  
  frontBottomContainer.add(text.footer[0], text.footer[1]);
  
  
  button.footer = new THREE.Object3D();
  button.footer.plane = new THREE.Mesh(new THREE.PlaneBufferGeometry(4, 0.6), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.000001 }));
  button.footer.text = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(`Скачать приложение`, 0.22), 3), new THREE.MeshBasicMaterial({ color: 0xffffff }));
  button.footer.text.geometry.center();
  button.footer.pic = new THREE.Mesh(new THREE.PlaneBufferGeometry(0.6, 0.6), pic[5]);
  button.footer.pic.position.x = -1.2;
  button.footer.text.position.x = 0.9;
  button.footer.plane.position.x = 0.5;
  button.footer.add(button.footer.plane, button.footer.text, button.footer.pic);
  button.footer.position.set(1.5, 1, 0);
  text.footer[0].add(button.footer);

  button.footer_1 = new THREE.Object3D();
  button.footer_1.plane = new THREE.Mesh(new THREE.PlaneBufferGeometry(4, 0.6), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.0000001 }));
  button.footer_1.text = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(`Скачать приложение`, 0.22), 3), new THREE.MeshBasicMaterial({ color: 0xffffff }));
  button.footer_1.text.geometry.center();
  button.footer_1.pic = new THREE.Mesh(new THREE.PlaneBufferGeometry(0.6, 0.6), pic[5]);
  button.footer_1.pic.position.x = -1.2;
  button.footer_1.text.position.x = 0.9;
  button.footer_1.plane.position.x = 0.5;
  button.footer_1.add(button.footer_1.plane, button.footer_1.text, button.footer_1.pic);
  button.footer_1.position.set(0.75, 0.5, 0);
  button.footer_1.scale.set(0.5, 0.5, 0)
  
  
  createSVG(3, 12, 0xffffff);
  svgPic[3].scale.set(0.008, -0.008, 1);
  svgPic[3].position.set(0, -0.9, 0);
  svgPic[3].material.opacity = 1;
  svgPic[3].plane = new THREE.Mesh(new THREE.PlaneBufferGeometry(150, 30), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.0000001 }));
  svgPic[3].plane.position.set(67, 10, 0);
  svgPic[3].add(svgPic[3].plane);


  text.footer[1].add(text.footer[1].line[3], button.footer_1, svgPic[3]);
  text.footer[1].line[3].position.set(12.5, -1.05, 0);
  
  
  
  text.introText = [];
  for (let i = 0; i < 4; i++) {
    text.introText[i] = { object: new THREE.Object3D(), text: [] };
    if (i > 0) {
      createSeparatedText(text.introText[i], i + 1, 0.28, 0.52, EuclidCircularARegular, 0x000000);
      text.introText[i].topPart = { object: new THREE.Object3D(), part: [] };
      text.introText[i].topPart.object.scale.set(0, 0, 1);
    } else {
      createSeparatedText(text.introText[i], i + 1, 0.224, 0.4, EuclidCircularARegular, 0x000000);
      text.introText[0].topPart = { object: new THREE.Object3D(), objectPos: [2.4, 1.3], text: [] };
      createSeparatedText(text.introText[0].topPart, 0, 0.61, 0.95, EuclidCircularAMedium, 0x000000);
    }
    text.introText[i].object.position.y = -10 * i * introInterval;
    text.introText[i].object.add(text.introText[i].topPart.object);
    startScreenContainer.add(text.introText[i].object);
  }
  
  
  
  
  
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0.65, 0, 0.65, Math.PI * 1.5, Math.PI * 2.5);
  shape.absarc(0, 0.65, 0.65, 0, Math.PI);
  shape.absarc(-0.65, 0, 0.65, Math.PI / 2, Math.PI * 1.5);
  shape.absarc(-0.65, 0, 0.7, Math.PI * 1.5, Math.PI / 2, true);
  shape.absarc(0, 0.65, 0.7, Math.PI, 0, true);
  shape.absarc(0.65, 0, 0.7, Math.PI * 2.5, Math.PI * 1.5, true);
  shape.lineTo(-0.65, -0.7);
  shape.lineTo(-0.65, -0.65);
  shape.lineTo(0.65, -0.65);
  text.introText[1].topPart.part[0] = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 24), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0 }));
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.2, Math.PI / 6 * 5, Math.PI / 6 * 13);
  shape.lineTo(0, 0.35);
  shape.lineTo(0, 0.26);
  shape.absarc(0, 0, 0.15, Math.PI / 6 * 13, Math.PI / 6 * 5, true);
  shape.lineTo(0, 0.26);
  shape.lineTo(0, 0.35);
  for (let i = 1; i < 4; i++) {
    text.introText[1].topPart.part[i] = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 12), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0 }));
    text.introText[1].topPart.part[i].position.set(-1.2 + 0.6 * i, -0.75, 0);
    text.introText[1].topPart.part[i].scale.set(0, 0, 1);
  }
  gsap.to(text.introText[1].topPart.part[2].position, { duration: 1.8, y: -2, ease: "none", repeat: -1 });
  gsap.to(text.introText[1].topPart.part[1].position, { duration: 1.8, y: -2, ease: "none", repeat: -1, delay: 0.6 });
  gsap.to(text.introText[1].topPart.part[3].position, { duration: 1.8, y: -2, ease: "none", repeat: -1, delay: 1.2 });
  gsap.to(text.introText[1].topPart.part[2].scale, { duration: 0.9, x: 1, y: 1, ease: "expo.out", repeat: -1, yoyo: true });
  gsap.to(text.introText[1].topPart.part[1].scale, { duration: 0.9, x: 1, y: 1, ease: "expo.out", repeat: -1, yoyo: true, delay: 0.6 });
  gsap.to(text.introText[1].topPart.part[3].scale, { duration: 0.9, x: 1, y: 1, ease: "expo.out", repeat: -1, yoyo: true, delay: 1.2 });
  text.introText[1].topPart.object.position.y = 2.1;
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 1.6, Math.PI * 0.42, Math.PI * 0.31);
  shape.absarc(0, 0, 1.65, Math.PI * 0.31, Math.PI * 0.42, true);
  text.introText[2].topPart.part[0] = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 48), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0 }));
  for (let i = 1; i < 7; i++) {
    text.introText[2].topPart.part[i] = new THREE.Mesh(new THREE.PlaneBufferGeometry(0.0001, 0.0001), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0 }));
  }
  text.introText[2].topPart.part[1].position.set(Math.cos(Math.PI * 0.42) * 1.625, Math.sin(Math.PI * 0.42) * 1.625, 0);
  text.introText[2].topPart.part[1].rotation.z = 4.5;
  text.introText[2].topPart.part[2].position.set(Math.cos(Math.PI * 0.31) * 1.625, Math.sin(Math.PI * 0.31) * 1.625, 0);
  text.introText[2].topPart.part[3].rotation.z = 2.3;
  text.introText[2].topPart.part[4].rotation.z = 2.3;
  text.introText[2].topPart.part[5].rotation.z = 4.3;
  text.introText[2].topPart.object.position.y = 2.1;
  text.introText[2].tween = 0;
  text.introText[2].animate = gsap.to(text.introText[2], { duration: 0.4, tween: 1, ease: "elastic.in", repeat: 1, yoyo: true, onUpdate: function() {
    shape = null;
    shape = new THREE.Shape();
    shape.absarc(0, 0, 0.025, Math.PI / 2, Math.PI * 1.5);
    shape.absarc(1.05 - 0.4 * text.introText[2].tween, 0, 0.025, Math.PI * 1.5, Math.PI * 2.5);
    text.introText[2].topPart.part[1].geometry.dispose();
    text.introText[2].topPart.part[1].geometry = new THREE.ShapeBufferGeometry(shape, 3);
    text.introText[2].topPart.part[1].geometry.NeedUpdate = true;
    shape = null;
    shape = new THREE.Shape();
    shape.absarc(0, 0, 0.025, Math.PI / 2, Math.PI * 1.5);
    shape.absarc(1.445 - 0.39 * text.introText[2].tween, 0, 0.025, Math.PI * 1.5, Math.PI * 2.5);
    text.introText[2].topPart.part[2].geometry.dispose();
    text.introText[2].topPart.part[2].geometry = new THREE.ShapeBufferGeometry(shape, 3);
    text.introText[2].topPart.part[2].geometry.NeedUpdate = true;
    text.introText[2].topPart.part[2].rotation.z = 4.25 - 0.1 * text.introText[2].tween;
    shape = null;
    shape = new THREE.Shape();
    shape.absarc(0, 0, 0.025, Math.PI / 2, Math.PI * 1.5);
    shape.absarc(0.48, 0, 0.025, Math.PI * 1.5, Math.PI * 2.5);
    text.introText[2].topPart.part[3].geometry.dispose();
    text.introText[2].topPart.part[3].geometry = new THREE.ShapeBufferGeometry(shape, 3);
    text.introText[2].topPart.part[3].geometry.NeedUpdate = true;
    text.introText[2].topPart.part[3].position.set(0.178 + 0.082 * text.introText[2].tween, 0.55 + 0.4 * text.introText[2].tween, 0);
    shape = null;
    shape = new THREE.Shape();
    shape.absarc(0, 0, 0.025, Math.PI / 2, Math.PI * 1.5);
    shape.absarc(0.36, 0, 0.025, Math.PI * 1.5, Math.PI * 2.5);
    text.introText[2].topPart.part[4].geometry.dispose();
    text.introText[2].topPart.part[4].geometry = new THREE.ShapeBufferGeometry(shape, 3);
    text.introText[2].topPart.part[4].geometry.NeedUpdate = true;
    text.introText[2].topPart.part[4].position.set(0.255 + 0.082 * text.introText[2].tween, 0.05 + 0.4 * text.introText[2].tween, 0);
    shape = null;
    shape = new THREE.Shape();
    shape.absarc(0, 0, 0.025, Math.PI / 2, Math.PI * 1.5);
    shape.absarc(1.35 - 0.5 * text.introText[2].tween, 0, 0.025, Math.PI * 1.5, Math.PI * 2.5);
    text.introText[2].topPart.part[5].geometry.dispose();
    text.introText[2].topPart.part[5].geometry = new THREE.ShapeBufferGeometry(shape, 3);
    text.introText[2].topPart.part[5].geometry.NeedUpdate = true;
    text.introText[2].topPart.part[5].position.set(0 + 0.082 * text.introText[2].tween, 0.3 + 0.4 * text.introText[2].tween, 0);
    shape = null;
    shape = new THREE.Shape();
    shape.absarc(0, 0, 0.025, Math.PI / 2, Math.PI * 1.5);
    shape.absarc(1.9 - 0.5 * text.introText[2].tween, 0, 0.025, Math.PI * 1.5, Math.PI * 2.5);
    text.introText[2].topPart.part[6].geometry.dispose();
    text.introText[2].topPart.part[6].geometry = new THREE.ShapeBufferGeometry(shape, 3);
    text.introText[2].topPart.part[6].geometry.NeedUpdate = true;
    text.introText[2].topPart.part[6].position.set(-0.15 + 0.082 * text.introText[2].tween, 0.9 + 0.4 * text.introText[2].tween, 0);
    text.introText[2].topPart.part[6].rotation.z = 4.5 + 0.07 * text.introText[2].tween;
  }, onComplete: function() {
    setTimeout(function() {
      text.introText[2].animate.restart();
    }, 1000);
  } });
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 0.35, 0, Math.PI * 2);
  shape.absarc(0, 0, 0.4, Math.PI * 2, 0, true);
  text.introText[3].topPart.part[0] = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 48), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0 }));
  for (let i = 1; i < 13; i++) {
    text.introText[3].topPart.part[i] = new THREE.Mesh(new THREE.PlaneBufferGeometry(0.0001, 0.0001), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0 }));
    text.introText[3].topPart.part[i].position.set(Math.cos(Math.PI / 6 * (i - 1)) * 0.6, Math.sin(Math.PI / 6 * (i - 1)) * 0.6, 0);
    text.introText[3].topPart.part[i].rotation.z = Math.PI / 6 * (i - 1);
  }
  text.introText[3].topPart.object.position.y = 2.1;
  text.introText[3].tween = 0;
  gsap.to(text.introText[3], { duration: 1, tween: 1, ease: "power1.inOut", repeat: -1, yoyo: true, onUpdate: function() {
    shape = null;
    shape = new THREE.Shape();
    shape.absarc(0, 0, 0.025, Math.PI / 2, Math.PI * 1.5);
    shape.absarc(0.8 + 0.7 * text.introText[3].tween, 0, 0.025, Math.PI * 1.5, Math.PI * 2.5);
    for (let i = 1; i < 13; i += 2) {
      text.introText[3].topPart.part[i].geometry.dispose();
      text.introText[3].topPart.part[i].geometry = new THREE.ShapeBufferGeometry(shape, 3);
      text.introText[3].topPart.part[i].geometry.NeedUpdate = true;
    }
    shape = null;
    shape = new THREE.Shape();
    shape.absarc(0, 0, 0.025, Math.PI / 2, Math.PI * 1.5);
    shape.absarc(1.5 - 0.7 * text.introText[3].tween, 0, 0.025, Math.PI * 1.5, Math.PI * 2.5);
    for (let i = 2; i < 13; i += 2) {
      text.introText[3].topPart.part[i].geometry.dispose();
      text.introText[3].topPart.part[i].geometry = new THREE.ShapeBufferGeometry(shape, 3);
      text.introText[3].topPart.part[i].geometry.NeedUpdate = true;
    }
  } });
  for (let i = 1; i < 4; i++) {
    for (let j = 0; j < text.introText[i].topPart.part.length; j++) {
     text.introText[i].topPart.object.add(text.introText[i].topPart.part[j]);
    }
  }
 
  
 
 
 
  
  
  text.finText = [];
  for (let i = 0; i < 4; i++) {
    text.finText[i] = { object: new THREE.Object3D(), text: [] };
  }
  createSeparatedText(text.finText[0], 10, 0.42, 0.7, EuclidCircularAMedium, 0x000000);
  text.finText[0].posY = [2, 3.9];
  createSeparatedText(text.finText[1], 11, 0.224, 0.42, EuclidCircularARegular, 0x595959);
  text.finText[1].posY = [-0.55, 1.5];
  createSeparatedText(text.finText[2], 12, 0.28, 0.52, EuclidCircularARegular, 0x595959);
  text.finText[2].posY = [-4.9, -2.3];
  text.finText[2].screenScale = [1, 0.82];
  createSeparatedText(text.finText[3], 23, 0.336, 0.65, EuclidCircularAMedium, 0x000000);
  text.finText[3].posY = [-17, -11.6];
  text.finText[3].screenScale = [1, 0.8];
  text.finText[3].alert = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(`Промокод скопирован`, 0.2), 3), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 0 }));
  text.finText[3].alert.geometry.center();
  text.finText[3].plane = new THREE.Mesh(new THREE.PlaneBufferGeometry(1.5, 0.7), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.0000001 }));
  text.finText[3].plane.position.set(1.7, -4.4, 0);
  text.finText[3].alert.position.set(1.7, -5.1, 0);
  text.finText[3].object.add(text.finText[3].plane, text.finText[3].alert);
  

 
 
  finScreenContainer.add(text.finText[0].object, text.finText[1].object, text.finText[2].object);
 
 
  //productContainer.posY = [-16, -80];
  
 // productContainer.position.y = -11.6;
  productContainer.products = new THREE.Object3D();
  
  
  
  productContainer.productPic = [];
  
  
  
  productContainer.touch = new THREE.Mesh(new THREE.PlaneBufferGeometry(200, 11.4), new THREE.MeshBasicMaterial({ color: 0xff0000, transparent: true, opacity: 0.0000001 }));
  productContainer.touch.position.y = -1.7;
  
  
  for (let i = 0; i < 5; i++) {
    productContainer.productPic[i] = new THREE.Object3D();
    productContainer.productPic[i].pic = new THREE.Mesh(new THREE.PlaneBufferGeometry(4.9, 6.93), pic[i]);
    productContainer.productPic[i].dot = new THREE.Mesh(new THREE.CircleBufferGeometry(0.04, 12), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0 }));
    productContainer.productPic[i].dot.position.set(-1 + 0.4 * i, 4.05, 0);
    productContainer.productPic[i].pic.material.opacity = 0;
    productContainer.productPic[i].add(productContainer.productPic[i].pic);
    productContainer.productPic[i].position.set(5.1 * i, 0, 0);
    productContainer.productPic[i].text = [];
    productContainer.productPic[i].text[0] = { object: new THREE.Object3D(), text: [] };
    createSeparatedText(productContainer.productPic[i].text[0], 13 + i, 0.252, 0.46, EuclidCircularAMedium, 0x000000);
    productContainer.productPic[i].text[0].object.position.set(5.1 * i, -3.7, 0);
    productContainer.productPic[i].text[1] = { object: new THREE.Object3D(), text: [] };
    createSeparatedText(productContainer.productPic[i].text[1], 18 + i, 0.196, 0.38, EuclidCircularARegular, 0xa6a6a6);
    productContainer.productPic[i].text[1].object.position.set(5.1 * i, productContainer.productPic[i].text[0].object.position.y + productContainer.productPic[i].text[0].text[(productContainer.productPic[i].text[0].text.length - 1)].pos.Y[0] - 0.2, 0);
    productContainer.add( productContainer.productPic[i].text[1].object);

    productContainer.add(productContainer.productPic[i].dot);
    productContainer.products.add(productContainer.productPic[i].text[1].object, productContainer.productPic[i].text[0].object, productContainer.productPic[i]);
  }
  productContainer.add(productContainer.products, productContainer.touch);
  
  productContainer.posY = [-11.6, -6.4];
  productContainer.screenScale = [1, 0.65];

  finScreenContainer.add(productContainer);
 
 
 
 
 
 
 
  text.inputText = [];
  for (let i = 0; i < 2; i++) {
    text.inputText[i] = { object: new THREE.Object3D(), text: [] };
    createSeparatedText(text.inputText[i], i + 5, 0.224, 0.4, EuclidCircularARegular, 0x595959);
    frontStartScreenContainer.add(text.inputText[i].object);
  }
  text.inputText[0].posY = [5.5, 4.1];
  text.inputText[1].posY = [3.9, 3];
  
  
  
  
  userInput.position.y = -40 * introInterval - 0.5;
  
  userInput.posY = userInput.position.y;
  userInput.ready = false;
  userInput.position.y -= 40;
  text.userText = [];
  
  text.userText[0] = new THREE.Object3D();
  text.userText[0].line = [];
  text.userText[0].line[0] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(``, 0.28), 3), new THREE.MeshBasicMaterial({ color: 0xe0e0e0, transparent: true, opacity: 1 }));
  text.userText[0].line[0].geometry.computeBoundingBox();
  text.userText[0].line[0].geometry.translate(-text.userText[0].line[0].geometry.boundingBox.max.x / 2, 0, 0);
  text.userText[0].line[0].position.y = 0.87;

  text.userText[0].line[1] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(``, 0.28), 3), new THREE.MeshBasicMaterial({ color: 0xe0e0e0, transparent: true, opacity: 1 }));
  text.userText[0].line[1].position.y = 0.35;
  text.userText[0].line[1].geometry.computeBoundingBox();
  text.userText[0].line[1].geometry.translate(-text.userText[0].line[1].geometry.boundingBox.max.x / 2, 0, 0);

  text.userText[0].line[2] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(``, 0.28), 3), new THREE.MeshBasicMaterial({ color: 0xe0e0e0, transparent: true, opacity: 1 }));
  
  
  text.userText[0].line[2].position.y = 0.35;

  text.userText[0].cursor = new THREE.Mesh(new THREE.PlaneBufferGeometry(0.02, 0.4), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 1 }));

  text.userText[0].cursor.position.y = 0.49;
  text.userText[0].cursor.scale.set(0, 0, 1);
  gsap.to(text.userText[0].cursor.material, { duration: 0.55, opacity: 0, ease: "power1.inOut", repeat: -1, yoyo: true });


  text.userText[0].add(text.userText[0].line[0], text.userText[0].line[1], text.userText[0].line[2], text.userText[0].cursor);
  
  
  text.userText[1] = new THREE.Object3D();
  text.userText[1].line = [];
  text.userText[1].line[0] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(``, 0.28), 3), new THREE.MeshBasicMaterial({ color: 0xe0e0e0, transparent: true, opacity: 1 }));
  text.userText[1].line[0].geometry.computeBoundingBox();
  text.userText[1].line[0].geometry.translate(-text.userText[1].line[0].geometry.boundingBox.max.x / 2, 0, 0);
  text.userText[1].line[0].position.y = 0.35;

  
  text.userText[1].cursor = new THREE.Mesh(new THREE.PlaneBufferGeometry(0.02, 0.4), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 1 }));

  text.userText[1].cursor.position.y = 0.49;
  text.userText[1].cursor.scale.set(0, 0, 1);
  gsap.to(text.userText[1].cursor.material, { duration: 0.55, opacity: 0, ease: "power1.inOut", repeat: -1, yoyo: true });


  text.userText[1].add(text.userText[1].line[0], text.userText[1].cursor);
  
  
  
  
 
  text.count = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(`50/50`, 0.18), 3), new THREE.MeshBasicMaterial({ color: 0xa6a6a6, transparent: true, opacity: 0 }));
  text.count.geometry.computeBoundingBox();
  text.count.posY = text.count.geometry.boundingBox.max.x / 2;
  text.count.geometry.dispose();
  text.count.geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(`0/50`, 0.18), 3);
  text.count.geometry.computeBoundingBox();
  text.count.geometry.translate(-text.count.geometry.boundingBox.max.x + text.count.posY, 0, 0);
  text.count.geometry.NeedUpdate = true;
  text.count.position.y = -0.5;





  text.userText[0].container = new THREE.Object3D();
  text.userText[0].newLine = [];
  for (let i = 0; i < 10; i++) {
    text.userText[0].newLine[i] = new THREE.Mesh(new THREE.PlaneBufferGeometry(0.000001, 0.000001), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 1 }));


    text.userText[0].container.add(text.userText[0].newLine[i]);
    
  }
  text.userText[0].container.visible = false;
  


  text.userText[1].container = new THREE.Object3D();
  text.userText[1].newLine = [];
  for (let i = 0; i < 10; i++) {
    text.userText[1].newLine[i] = new THREE.Mesh(new THREE.PlaneBufferGeometry(0.000001, 0.000001), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 1 }));
  
  
    text.userText[1].container.add(text.userText[1].newLine[i]);
  
  }
  text.userText[1].container.visible = false;



  waterCenterContainer.add(text.userText[0].container, text.userText[1].container);

  
  userInput.add(text.userText[0], text.userText[1], text.count);
  
  
  userInput.field = [];
  
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-2.8, 0, 0.75, Math.PI * 0.5, Math.PI * 1.5);
  shape.absarc(2.8, 0, 0.75, Math.PI * 1.5, Math.PI * 2.5);
  
  
  
  userInput.field[0] = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 36), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 0 }));
  userInput.field[0].geometry.translate(0, 0.75, 0);
 
 
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-6.2, 0, 0.5, Math.PI * 0.5, Math.PI * 1.5);
  shape.absarc(6.2, 0, 0.5, Math.PI * 1.5, Math.PI * 2.5);
  userInput.field[1] = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 36), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 0 }));
  userInput.field[1].geometry.translate(0, 0.5, 0);

 
  startScreenContainer.add(userInput.field[0], userInput.field[1]);
  
//frontStartScreenContainer.add(userInput.field);
  
  //startScreenContainer.add(CSSField.object);
  //CSSField.object.position.y = -40 * introInterval;
  frontStartScreenContainer.add(userInput);
  
  ///////////////////////////
  
  
  
  
  keyboard.ready = false;
  keyboard.numButtons = new THREE.Object3D();
  keyboard.EN = new THREE.Object3D();
  keyboard.RU = new THREE.Object3D();
  keyboard.bottom = new THREE.Object3D();
  keyboard.button = [];
  keyboard.upperCase = false;
  keyboard.UpperCaseStatic = false;
  
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-0.65, 0.85, 0.2, Math.PI * 0.5, Math.PI);
  shape.absarc(-0.65, -0.85, 0.2, Math.PI, Math.PI * 1.5);
  shape.absarc(0.65, -0.85, 0.2, Math.PI * 1.5, Math.PI * 2);
  shape.absarc(0.65, 0.85, 0.2, Math.PI * 2, Math.PI * 2.5);


  
  for (let i = 0; i < 42; i++) {
    keyboard.button[i] = new THREE.Object3D();
    keyboard.button[i].key = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 3), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.00000001 }));
    keyboard.button[i].key.position.z = -0.01;
    keyboard.button[i].symbol = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(keyboardData[i], 0.8), 3), BLACK_MATERIAL);
    keyboard.button[i].symbol.geometry.computeBoundingBox();
    keyboard.button[i].symbol.geometry.translate(-keyboard.button[i].symbol.geometry.boundingBox.max.x / 2, -0.4, 0);
    keyboard.button[i].add(keyboard.button[i].key, keyboard.button[i].symbol);
    
    
  }
  
  
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-3.1, 0, 1, Math.PI * 0.5, Math.PI * 1.5);
  shape.absarc(3.1, 0, 1, Math.PI * 1.5, Math.PI * 2.5);
  keyboard.button[42] = new THREE.Object3D();
  keyboard.button[42].key = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 12), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.1 }));
  keyboard.button[42].key.position.z = -0.01;
  keyboard.button[42].symbol = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(``, 0.9), 3), BLACK_MATERIAL);

  keyboard.button[42].add(keyboard.button[42].key, keyboard.button[42].symbol);

  keyboard.bottom.add(keyboard.button[42]);
  keyboard.button[42].position.y = -2.3;
  
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-0.57, 0.85, 0.2, Math.PI * 0.5, Math.PI);
  shape.absarc(-0.57, -0.85, 0.2, Math.PI, Math.PI * 1.5);
  shape.absarc(0.57, -0.85, 0.2, Math.PI * 1.5, Math.PI * 2);
  shape.absarc(0.57, 0.85, 0.2, Math.PI * 2, Math.PI * 2.5);

  
  for (let i = 43; i < 76; i++) {
    keyboard.button[i] = new THREE.Object3D();
    keyboard.button[i].key = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 3), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.00000001 }));
    keyboard.button[i].key.position.z = -0.01;
    keyboard.button[i].symbol = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(keyboardData[i], 0.8), 3), BLACK_MATERIAL);
    keyboard.button[i].symbol.geometry.computeBoundingBox();
    keyboard.button[i].symbol.geometry.translate(-keyboard.button[i].symbol.geometry.boundingBox.max.x / 2, -0.4, 0);
    keyboard.button[i].add(keyboard.button[i].key, keyboard.button[i].symbol);
  
    keyboard.RU.add(keyboard.button[i]);
  }
  
  
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-1.075, 0, 1, Math.PI * 0.5, Math.PI * 1.5);
  shape.absarc(1.075, 0, 1, Math.PI * 1.5, Math.PI * 2.5);
  keyboard.button[76] = new THREE.Object3D();
  keyboard.button[76].key = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 12), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.1 }));
  keyboard.button[76].key.position.z = -0.01;
  keyboard.button[76].symbol = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(`RU`, 0.55), 3), BLACK_MATERIAL);
  keyboard.button[76].symbol.geometry.computeBoundingBox();
  keyboard.button[76].symbol.geometry.translate(-keyboard.button[76].symbol.geometry.boundingBox.max.x / 2, -0.275, 0);
    
  keyboard.button[76].add(keyboard.button[76].key, keyboard.button[76].symbol);

  keyboard.bottom.add(keyboard.button[76]);
  keyboard.button[76].position.set(-6.375, -2.3, 0);

  keyboard.button[77] = new THREE.Object3D();
  keyboard.button[77].key = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 12), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 0.1 }));
  keyboard.button[77].key.position.z = -0.01;
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-0.7, 0, 0.06, Math.PI * 0.75, Math.PI * 1.25);
  shape.absarc(-0.2, -0.3, 0.06, Math.PI * 1.25, Math.PI * 2.25);
  shape.lineTo(-0.5, -0.06);
  shape.absarc(0.7, 0, 0.06, Math.PI * 1.5, Math.PI * 2);
  shape.absarc(0.7, 0.3, 0.06, 0, Math.PI);
  shape.lineTo(0.64, 0.06);
  shape.lineTo(-0.5, 0.06);
  shape.absarc(-0.2, 0.3, 0.06, Math.PI * 1.75, Math.PI * 2.75);
  keyboard.button[77].symbol = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 3), BLACK_MATERIAL);

  keyboard.button[77].symbol.scale.set(0.8, 0.8, 1);
  
  keyboard.button[77].add(keyboard.button[77].key, keyboard.button[77].symbol);
  keyboard.bottom.add(keyboard.button[77]);
  keyboard.button[77].position.set(6.375, -2.3, 0);





  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-0.65, 0, 1, Math.PI * 0.5, Math.PI * 1.5);
  shape.absarc(0.65, 0, 1, Math.PI * 1.5, Math.PI * 2.5);

  keyboard.button[78] = new THREE.Object3D();
  keyboard.button[78].key = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 12), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.1 }));
  keyboard.button[78].key.position.z = -0.01;
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0.6, 0.06, Math.PI * 0.25, Math.PI * 0.75);
  shape.absarc(-0.6, 0, 0.06, Math.PI * 0.75, Math.PI * 1.5);
  shape.lineTo(-0.4, -0.06);
  shape.absarc(-0.34, -0.5, 0.06, Math.PI, Math.PI * 1.5);
  shape.lineTo(-0.28, -0.44);
  shape.lineTo(-0.28, 0.06);
  shape.lineTo(-0.46, 0.06);
  shape.lineTo(0, 0.54);
  shape.lineTo(0.46, 0.06);
  shape.lineTo(0.28, 0.06);
  shape.lineTo(0.28, -0.44);
  shape.lineTo(-0.28, -0.44);
  shape.lineTo(-0.4, -0.56);
  shape.absarc(0.34, -0.5, 0.06, Math.PI * 1.5, Math.PI * 2);
  
  shape.lineTo(0.4, -0.06);
  shape.absarc(0.6, 0, 0.06, Math.PI * 1.5, Math.PI * 2.25);

/*shape.lineTo(-0.5, 0.06);
  shape.absarc(-0.2, 0.3, 0.06, Math.PI * 1.75, Math.PI * 2.75);*/
  
  
  
  
  
  
  keyboard.button[78].symbol = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 3), BLACK_MATERIAL);
  keyboard.button[78].symbol.scale.set(0.8, 0.8, 1);

  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0.6, 0.06, Math.PI * 0.25, Math.PI * 0.75);
  shape.absarc(-0.6, 0, 0.06, Math.PI * 0.75, Math.PI * 1.5);
  shape.lineTo(-0.4, -0.06);
  shape.absarc(-0.34, -0.5, 0.06, Math.PI, Math.PI * 1.5);
  
  shape.absarc(0.34, -0.5, 0.06, Math.PI * 1.5, Math.PI * 2);
  
  shape.lineTo(0.4, -0.06);
  shape.absarc(0.6, 0, 0.06, Math.PI * 1.5, Math.PI * 2.25);

  keyboard.button[78].symbol2 = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 3), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0 }));
  keyboard.button[78].symbol2.scale.set(0.8, 0.8, 1);

  keyboard.button[78].add(keyboard.button[78].key, keyboard.button[78].symbol, keyboard.button[78].symbol2);
  keyboard.bottom.add(keyboard.button[78]);
  keyboard.button[78].position.set(-6.8, 0, 0);



  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-0.65, 0, 1, Math.PI * 0.5, Math.PI * 1.5);
  shape.absarc(0.65, 0, 1, Math.PI * 1.5, Math.PI * 2.5);

  keyboard.button[79] = new THREE.Object3D();
  keyboard.button[79].key = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 12), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.1 }));
  keyboard.button[79].key.position.z = -0.01;
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-0.8, 0, 0.06, Math.PI / 6 * 5, Math.PI / 6 * 7);
  shape.absarc(-0.45, -0.45, 0.06, Math.PI / 6 * 7, Math.PI * 1.5);

  shape.absarc(0.6, -0.45, 0.06, Math.PI * 1.5, Math.PI * 2);
  shape.lineTo(0.6, -0.39);
  shape.lineTo(-0.42, -0.39);
  shape.lineTo(-0.74, 0);
  shape.lineTo(-0.42, 0.39);
  shape.lineTo(0.54, 0.39);
  shape.lineTo(0.54, -0.39);
  shape.lineTo(0.66, -0.45);
 
  shape.absarc(0.6, 0.45, 0.06, 0, Math.PI * 0.5);
  shape.absarc(-0.45, 0.45, 0.06, Math.PI * 0.5, Math.PI / 6 * 5);

  keyboard.button[79].symbol = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 3), BLACK_MATERIAL);
 
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-0.25, 0, 0.05, Math.PI * 0.5, Math.PI * 1.5);
  shape.lineTo(-0.05, -0.05);
  shape.absarc(0, -0.25, 0.05, Math.PI, Math.PI * 2);
  shape.lineTo(0.05, -0.05);
  shape.absarc(0.25, 0, 0.05, Math.PI * 1.5, Math.PI * 2.5);
  shape.lineTo(0.05, 0.05);
  shape.absarc(0, 0.25, 0.05, 0, Math.PI);
  shape.lineTo(-0.05, 0.05);
  
  
  keyboard.button[79].symbol.scale.set(0.8, 0.8, 1);

  
  keyboard.button[79].symbol2 = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 3), BLACK_MATERIAL);
  keyboard.button[79].symbol2.rotation.z = Math.PI / 4;
  keyboard.button[79].symbol2.scale.set(0.8, 0.8, 1);

  keyboard.button[79].add(keyboard.button[79].key, keyboard.button[79].symbol, keyboard.button[79].symbol2);




  keyboard.bottom.add(keyboard.button[79]);
  keyboard.button[79].position.set(6.8, 0, 0);
  for (let i = 0; i < 10; i++) {
    keyboard.button[i].position.x = -7.65 + 1.7 * i;
    keyboard.numButtons.add(keyboard.button[i]);
  }
  for (let i = 10; i < 36; i++) {
    keyboard.EN.add(keyboard.button[i]);
  }
  for (let i = 36; i < 42; i++) {
    keyboard.bottom.add(keyboard.button[i]);
  }
  for (let i = 0; i < 10; i++) {
    keyboard.button[i + 10].position.x = -7.65 + 1.7 * i;
  }
  for (let i = 0; i < 9; i++) {
    keyboard.button[i + 20].position.set(-6.8 + 1.7 * i, -2.1, 0);
  }
  for (let i = 0; i < 7; i++) {
    keyboard.button[i + 29].position.set(-5.1 + 1.7 * i, -4.2, 0);
  }
  for (let i = 0; i < 6; i++) {
    keyboard.button[i + 36].position.set(-4.25 + 1.7 * i, 0, 0);
  }
  for (let i = 0; i < 11; i++) {
    keyboard.button[i + 43].position.x = -7.73 + 1.545 * i;
  }
  for (let i = 0; i < 11; i++) {
    keyboard.button[i + 54].position.set(-7.73 + 1.545 * i, -2.1, 0);
  }
  for (let i = 0; i < 11; i++) {
    keyboard.button[i + 65].position.set(-7.73 + 1.545 * i, -4.2, 0);
  }
  keyboard.numButtons.position.y = 7.1;
  keyboard.EN.position.y = 5
  keyboard.RU.position.y = 5;
  keyboard.bottom.position.y = -1.3;
  keyboard.touch = new THREE.Mesh(new THREE.PlaneBufferGeometry(100, 50), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.0000001 }));
  
  keyboard.touch.position.set(0, 34.5, 0);
  
  keyboard.touch_1 = new THREE.Mesh(new THREE.PlaneBufferGeometry(800, 50), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.0000001 }));
  
  keyboard.touch_1.position.x = -409;
  keyboard.touch_2 = new THREE.Mesh(new THREE.PlaneBufferGeometry(800, 50), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.0000001 }));
  
  keyboard.touch_2.position.x = 409;


  keyboard.add(keyboard.touch, keyboard.touch_1, keyboard.touch_2, keyboard.numButtons, keyboard.EN, keyboard.bottom, keyboard.RU);

  keyboard.position.y = -4;
  
  keyboard.scale.set(0.42, 0.42, 1);
  
  frontBottomContainer.add(keyboard);
  
  
  keyboard.EN.position.y = -400;
  
  button.goMovie = new THREE.Object3D();
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-3.2, 0.5, 0.12, Math.PI * 0.5, Math.PI);
  shape.absarc(-3.2, -0.5, 0.12, Math.PI, Math.PI * 1.5);
  shape.absarc(3.2, -0.5, 0.12, Math.PI * 1.5, Math.PI * 2);
  shape.absarc(3.2, 0.5, 0.12, 0, Math.PI * 0.5);
  shape.lineTo(3.2, 0.59);
  shape.absarc(3.2, 0.5, 0.09, Math.PI * 0.5, 0, true);
  shape.absarc(3.2, -0.5, 0.09, Math.PI * 2, Math.PI * 1.5, true);
  shape.absarc(-3.2, -0.5, 0.09, Math.PI * 1.5, Math.PI, true);
  shape.absarc(-3.2, 0.5, 0.09, Math.PI, Math.PI * 0.5, true);
  shape.lineTo(3.2, 0.59);
  shape.lineTo(3.2, 0.62);



  
  button.goMovie.border = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 3), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 0 }));
 
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(3.2, 0.5, 0.09, Math.PI * 0.5, 0, true);
  shape.absarc(3.2, -0.5, 0.09, Math.PI * 2, Math.PI * 1.5, true);
  shape.absarc(-3.2, -0.5, 0.09, Math.PI * 1.5, Math.PI, true);
  shape.absarc(-3.2, 0.5, 0.09, Math.PI, Math.PI * 0.5, true);

  button.goMovie.plane = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 3), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.0000001 }));
  button.goMovie.plane.visible = false;
  button.goMovie.text = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(`Растворить в воде`, 0.23), 3), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 0 }));

  button.goMovie.text.geometry.center();
  
  button.goMovie.text.position.z = 0;
  button.goMovie.position.y = 1.5;
  
  
  
  button.goMovie.add(button.goMovie.border, button.goMovie.plane, button.goMovie.text);

  frontBottomContainer.add(button.goMovie);
  button.product_2.onHover = false;
  button.product_3.onHover = false;
  button.popUp.onHover = false;
  button.goMovie.onHover = false;
  
  
  graphicsReady = true;
  onWindowResize();
  setTimeout(function() {
    endLoad();
  }, 300);
}
function createSeparatedText(chosen, chosenText, size, lineHeight, font, color) {
  let geometry;
  let portraitLine = -lineHeight;
  let albumLine = -lineHeight;
  let lineStartPortrait = 0;
  let lineStartAlbum = 0;
  let wordSeparatePoint = 0;
  let wordCount = 0;
  let currentWord = 0;
  let lineWidth;
  for (let i = 0; i < waterText[chosenText].length; i++) {
    if (waterText[chosenText][i] == `<` || waterText[chosenText][i] == `|` || waterText[chosenText][i] == `>` || waterText[chosenText][i] == ` `) {
      chosen.text[wordCount] = new THREE.Mesh(new THREE.ShapeBufferGeometry(font.generateShapes(waterText[chosenText].slice(wordSeparatePoint, i), size), 3), new THREE.MeshBasicMaterial({ color: color, transparent: true, opacity: 0 }));
      chosen.text[wordCount].geometry.computeBoundingBox();
      chosen.text[wordCount].geometry.translate(-chosen.text[wordCount].geometry.boundingBox.max.x / 2, 0, 0);
      chosen.text[wordCount].pos = { X: [], Y: [], startOffset: [] };
      chosen.object.add(chosen.text[wordCount]);
      wordSeparatePoint = i + 1;
      if (waterText[chosenText][i] == `<` || waterText[chosenText][i] == `|`) {
        geometry = new THREE.ShapeBufferGeometry(font.generateShapes(clearText(waterText[chosenText].slice(lineStartPortrait, i)), size), 3);
        geometry.computeBoundingBox();
        lineWidth = -geometry.boundingBox.max.x / 2;
        chosen.text[wordCount].pos.X[0] = -lineWidth - chosen.text[wordCount].geometry.boundingBox.max.x;
        chosen.text[wordCount].pos.Y[0] = portraitLine;
        chosen.text[wordCount].pos.startOffset[0] = -lineHeight + Math.random() * lineHeight * 2;
        currentWord = wordCount - 1;
        for (let j = i; j >= lineStartPortrait; j--) {
          if (waterText[chosenText][j] == ` ` || waterText[chosenText][j] == `>`) {
            geometry.dispose();
            geometry = new THREE.ShapeBufferGeometry(font.generateShapes(clearText(waterText[chosenText].slice(lineStartPortrait, j)), size), 3);
            geometry.computeBoundingBox();
            chosen.text[currentWord].pos.X[0] = lineWidth + geometry.boundingBox.max.x - chosen.text[currentWord].geometry.boundingBox.max.x;
            chosen.text[currentWord].pos.Y[0] = portraitLine;
            chosen.text[currentWord].pos.startOffset[0] = -lineHeight + Math.random() * lineHeight * 2;
            currentWord --;
          }
          if (j == lineStartPortrait) {
            chosen.text[currentWord + 1].pos.X[0] = lineWidth + chosen.text[currentWord + 1].geometry.boundingBox.max.x;
            chosen.text[currentWord + 1].pos.Y[0] = portraitLine;
            chosen.text[currentWord + 1].pos.startOffset[0] = -lineHeight + Math.random() * lineHeight * 2;
          }
        }
        lineStartPortrait = i + 1
        portraitLine -= lineHeight;
      }
      if (waterText[chosenText][i] == `>` || waterText[chosenText][i] == `|`) {
        geometry = new THREE.ShapeBufferGeometry(font.generateShapes(clearText(waterText[chosenText].slice(lineStartAlbum, i)), size), 3);
        geometry.computeBoundingBox();
        lineWidth = -geometry.boundingBox.max.x / 2;
        chosen.text[wordCount].pos.X[1] = -lineWidth - chosen.text[wordCount].geometry.boundingBox.max.x;
        chosen.text[wordCount].pos.Y[1] = albumLine;
        chosen.text[wordCount].pos.startOffset[1] = -lineHeight + Math.random() * lineHeight * 2;
        currentWord = wordCount - 1;
        for (let j = i; j >= lineStartAlbum; j--) {
          if (waterText[chosenText][j] == ` ` || waterText[chosenText][j] == `<`) {
            geometry.dispose();
            geometry = new THREE.ShapeBufferGeometry(font.generateShapes(clearText(waterText[chosenText].slice(lineStartAlbum, j)), size), 3);
            geometry.computeBoundingBox();
            chosen.text[currentWord].pos.X[1] = lineWidth + geometry.boundingBox.max.x - chosen.text[currentWord].geometry.boundingBox.max.x;
            chosen.text[currentWord].pos.Y[1] = albumLine;
            chosen.text[currentWord].pos.startOffset[1] = -lineHeight + Math.random() * lineHeight * 2;
            currentWord--;
          }
          if (j == lineStartAlbum) {
            chosen.text[currentWord + 1].pos.X[1] = lineWidth + chosen.text[currentWord + 1].geometry.boundingBox.max.x;
            chosen.text[currentWord + 1].pos.Y[1] = albumLine;
            chosen.text[currentWord + 1].pos.startOffset[1] = -lineHeight + Math.random() * lineHeight * 2;
          }
        }
        lineStartAlbum = i + 1
        albumLine -= lineHeight;
      }
      wordCount++;
    }
  }
  for (let i = 0; i < wordCount; i++) {
    chosen.text[i].showDelay = 1 + Math.random();
    chosen.text[i].position.x = chosen.text[i].pos.X[0];
  }
}
function clearText(line) {
  let newLine = ``;
  for (let i = 0; i < line.length; i++) {
    if (line[i] == `<` || line[i] == `>` || line[i] == ` `) {
      newLine += ` `;
    } else {
      newLine += line[i];
    }
  }
  return newLine;
}
let onIntro = false;
function showIntroScreen() {
  water.position.y = -11;
  text.introText[0].topPart.object.position.y = text.introText[0].topPart.objectPos[screenStat];
  const showIntroTimer = { step: 0 };
  gsap.to(water.position, { duration: 1.5, y: -6, ease: "power1.inOut" });
  gsap.to(showIntroTimer, { duration: 1.5, step: 1, ease: "power1.out", onUpdate: function() {
    showSeparatedText(text.introText[currentIntroScreen].topPart, showIntroTimer.step);
  }, onComplete: function() {
    showIntroTimer.step = 0;
    gsap.to(water.position, { duration: 0.5, y: -1, ease: "power1.inOut", delay: 1 });
    gsap.to(showIntroTimer, { duration: 1.5, step: 1, ease: "power1.out", onUpdate: function() {
      showSeparatedText(text.introText[currentIntroScreen], showIntroTimer.step);
    }, onComplete: function() {
      gsap.to(icon.scroll.scale, { duration: 0.3, x: 1, y: 1, ease: "power1.out" });
      gsap.to(icon.scroll.material, { duration: 0.3, opacity: 1, ease: "power1.out" });
      water.position.y = 1;
      startScreenContainer.startY = 0;
      startScreenPan = true;
      startScreenReady = true;
      onIntro = true;
      
     // oldDeltaY = currentDelta;
    } });
  } });
  gsap.from(text.introText[currentIntroScreen].object.position, { duration: 3, y: -2, ease: "power1.out" });
}
function showSeparatedText(chosen, step) {
  for (let i = 0; i < chosen.text.length; i++) {
    if (step - (1 - 1 / chosen.text[i].showDelay) >= 0) {
      
      
      
      chosen.text[i].material.opacity = (step - (1 - 1 / chosen.text[i].showDelay)) * chosen.text[i].showDelay;
      chosen.text[i].scale.set((step - (1 - 1 / chosen.text[i].showDelay)) * chosen.text[i].showDelay, (step - (1 - 1 / chosen.text[i].showDelay)) * chosen.text[i].showDelay, 1);

      
    } else {
    
      chosen.text[i].material.opacity = 0;
      chosen.text[i].scale.set(0, 0, 1);
    }
    chosen.text[i].position.set(chosen.text[i].pos.X[screenStat], chosen.text[i].pos.Y[screenStat] + chosen.text[i].pos.startOffset[screenStat] * (1 - step), 0);
  }
}


function showSoundButton() {
  gsap.to(pic[6], { duration: 0.5, opacity: 1, ease: "none" });

}


function endLoad() {
  gsap.to(text.loading.material, { duration: 0.5, opacity: 0, ease: "none" });
  gsap.to(text.loading.position, { duration: 0.5, y: text.loading.position.y - 0.4, ease: "power1.inOut" });
  goBlur = true;
  blurTween = gsap.to([vblur.uniforms.v, hblur.uniforms.h], { duration: 0.5, value: 0.5, ease: "power2.out", onUpdate: function() {
    vblur.uniforms.v.value = vblur.uniforms.v.value / window.innerHeight * window.devicePixelRatio * mainScene.scale.x;
    hblur.uniforms.h.value = hblur.uniforms.h.value / window.innerWidth * window.devicePixelRatio * mainScene.scale.x;
  }, onComplete: function() {
    blurTween.kill();
    blurTween = null;
    goBlur = false;
    document.body.addEventListener('touchend', onDocumentTouchEnd, false);
    document.body.addEventListener('wheel', onWheel, false);

    document.body.addEventListener('touchstart', onDocumentTouchStart, false);
    document.body.addEventListener('mousedown', onDocumentMouseDown, false);
    document.body.addEventListener('mousemove', onDocumentMouseMove, false);
  

   //if (true) {
   if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
      showPopUp();
    } else {
      ios = false;
     showIntro();
      //  goFin();
    }
    
  } });
}
function showPopUp() {
  gsap.to(popUp.plane.material, { duration: 0.3, opacity: 0.85, ease: "none" });
  
  gsap.to([svgPic[1].material, svgPic[2].material], { duration: 0.5, opacity: 1, ease: "none", onUpdate: function() {

    showSeparatedText(popUp.text_1, svgPic[2].material.opacity);
    showSeparatedText(popUp.text_2, svgPic[2].material.opacity);
   
 
  }, onComplete: function() {
    gsap.to([button.popUp.border.material, button.popUp.text.material], { duration: 0.2, opacity: 1, ease: "none", onComplete: function() {
      button.popUp.ready = true;
    } });

  } });
}
function hidePopUp() {
  
  gsap.to(popUp.plane.material, { duration: 0.3, opacity: 0, ease: "none" });
 // gsap.to(button.popUp.scale, { duration: 0.15, x: 0.9, y: 0.9, ease: "power1.out", repeat: 1, yoyo: true, onComplete: function() {
    gsap.to([button.popUp.border.material, button.popUp.text.material], { duration: 0.3, opacity: 0, ease: "none" });

//  } });
  gsap.to([svgPic[1].material, svgPic[2].material], { duration: 0.5, opacity: 0, ease: "none", onUpdate: function() {
    showSeparatedText(popUp.text_1, svgPic[2].material.opacity);
    showSeparatedText(popUp.text_2, svgPic[2].material.opacity);
    
  }, onComplete: function() {
    popUp.visible = false;
    showIntro();
  } });

}



function showIntro() {
  
  const changeGradientTimer = { step: 0 };
  gsap.to(changeGradientTimer, { duration: 3, step: 30, ease: "none", onUpdate: function() {
    document.getElementById('gradient').style.background = `linear-gradient(rgba(255, 255, 255, 0.95) ${changeGradientTimer.step}%, rgba(48, 174, 191, 0.8))`;

  } });
  showIntroScreen();
}
let startScreenPan = false;
let startScreenReady = false;
let stopDrag = false;
let oldDeltaY = 0;
let oldDeltaX = 0;
let finScreenPan = false;
let finScreenReady = false;
let onFinScreen = false;
let onIntroScreen = true;
let scrollProduct = false;
let productScrollReady = false;
let finScrollCount = -1;
let checkIntroEndOnce = false;
let hammertime = new Hammer(document.body);
hammertime.get('pan').set({ enable: true }); 
hammertime.get('pan').set({ direction: Hammer.DIRECTION_ALL });
hammertime.on('pan', function(event) {
  if (((!startScreenPan || !startScreenReady) && !onFinScreen) || ((!finScreenPan || !finScreenReady) && !onIntroScreen)) {
    oldDeltaY = event.deltaY * (Math.tan(mainCamera.fov * Math.PI / 360) * 24 / window.innerHeight);
  }
  if (event.eventType == 4 && !startScreenPan && !startScreenReady && !finScreenPan && !finScreenReady) {
    oldDeltaY = 0;
    oldDeltaX = 0;
  }
  if (finScreenReady && !finScreenPan && event.eventType == 2 && !stopDrag) {
    finScreenPan = true;
    oldDeltaX = event.deltaX * (Math.tan(mainCamera.fov * Math.PI / 360) * 24 / window.innerHeight);

    oldDeltaY = event.deltaY * (Math.tan(mainCamera.fov * Math.PI / 360) * 24 / window.innerHeight);
    if (finScreenContainer.tween !== null && finScreenContainer.tween !== undefined) {
      finScreenContainer.tween.kill();
      finScreenContainer.tween = null;
    }
    if (productContainer.tween !== null && productContainer.tween !== undefined) {
      productContainer.tween.kill();
      productContainer.tween = null;
    }
  }
 /* if (Math.abs(event.deltaY) < Math.abs(event.deltaX) && true && productScrollReady) {
    //alert('ok')
    scrollProduct = true;
  }*/
  if (finScreenPan && finScreenReady) {
    if (Math.abs(event.deltaY) > Math.abs(event.deltaX)) {
      finScreenContainer.position.y += oldDeltaY - event.deltaY * (Math.tan(mainCamera.fov * Math.PI / 360) * 24 / window.innerHeight);
      
    } else if (Math.abs(event.deltaY) < Math.abs(event.deltaX) && productScrollReady) {
      productContainer.products.position.x -= oldDeltaX - event.deltaX * (Math.tan(mainCamera.fov * Math.PI / 360) * 24 / window.innerHeight);
      if (productContainer.tween !== null && productContainer.tween !== undefined) {
        productContainer.tween.kill();
        productContainer.tween = null;
      }
    }
   // const screenOrder = Math.floor(finScreenContainer.position.y / (10 * finInterval));
   // const screenPos = finScreenContainer.position.y % (10 * finInterval);
   // animateFinScroll();
    oldDeltaY = event.deltaY * (Math.tan(mainCamera.fov * Math.PI / 360) * 24 / window.innerHeight);
    oldDeltaX = event.deltaX * (Math.tan(mainCamera.fov * Math.PI / 360) * 24 / window.innerHeight);

  
      if (productScrollReady) {
        if (event.eventType == 4 || productContainer.products.position.x > 1 || productContainer.products.position.x < -4 * 5.1 - 1) {
          if (productContainer.tween !== null && productContainer.tween !== undefined) {
            productContainer.tween.kill();
            productContainer.tween = null;
          }
        //  alert('ok')
          stopDrag = true;
          productScrollReady = false;
          let direction = Math.floor(productContainer.products.position.x / 5.1) * 5.1;
        //  alert(direction)
        
          if ((event.deltaX > 0 && productContainer.products.position.x < 0) || productContainer.products.position.x < -4 * 5.1) {
            //alert('>')
            direction = Math.ceil(productContainer.products.position.x / 5.1) * 5.1;
          }
          if (productContainer.tween !== null && productContainer.tween !== undefined) {
            productContainer.tween.kill();
            productContainer.tween = null;
          }
          productContainer.tween = gsap.to(productContainer.products.position, { duration: 0.5, x: direction, ease: "power2.out", onUpdate: function() {
            //animateProducts();
            
          }, onComplete: function() {
            
            if (productContainer.tween !== null && productContainer.tween !== undefined) {
              productContainer.tween.kill();
              productContainer.tween = null;
            }
            
            
           // scrollProduct
            
           // oldDeltaY = 0;
            oldDeltaX = 0;
          } });
        }
      }
      
      if (event.eventType == 2) {
        if (productContainer.touchTween !== null && productContainer.touchTween !== undefined) {
          productContainer.touchTween.kill();
          productContainer.touchTween = null;
        }
        gsap.to(icon.scroll.scale, { duration: 0.3, x: 0, y: 0, ease: "power1.out" });
        gsap.to(icon.scroll.material, { duration: 0.3, opacity: 0, ease: "none" });
        // startScreenReady = false;
      
      }
      
    if (screenStat == 0) {
      if (event.eventType == 4 || finScreenContainer.position.y < -1 || finScreenContainer.position.y > 4 * 10 * finInterval[0] + 1) {
        productScrollReady = false;
        
        
        stopDrag = true;
        let nextDirection = Math.ceil(finScreenContainer.position.y / (10 * finInterval[0])) * 10 * finInterval[0];
        if ((event.deltaY > 0 && finScreenContainer.position.y > 0) || finScreenContainer.position.y > (30 * finInterval[0])) {
          nextDirection = Math.floor(finScreenContainer.position.y / (10 * finInterval[0])) * 10 * finInterval[0];
        }
        finScreenPan = false;
        finScreenContainer.tween = gsap.to(finScreenContainer.position, { duration: 1 * Math.abs(nextDirection - finScreenContainer.position.y) / (10 * finInterval[0]), y: nextDirection, ease: "power2.out", onUpdate: function() {
         // animateFinScroll();
        }, onComplete: function() {
          if (finScreenContainer.tween !== null && finScreenContainer.tween !== undefined) {
            finScreenContainer.tween.kill();
            finScreenContainer.tween = null;
          }
          if (finScreenContainer.position.y == 30 * finInterval[0] || finScreenContainer.position.y == 0) {
            //gsap.to(icon.scroll.scale, { duration: 0.3, x: 1, y: 1, ease: "power1.out" });
            //gsap.to(icon.scroll.material, { duration: 0.3, opacity: 1, ease: "none" });
          
          }
          if (finScreenContainer.position.y == 30 * finInterval[0]) {
            finScrollCount ++;
            
          }
          if (finScreenContainer.position.y == 40 * finInterval[0] && finScrollCount == 2) {
            finScrollCount --;
          }
          oldDeltaY = 0;
         // oldDeltaX = 0;
        } });
      }
    } else {
      if (event.eventType == 4 || finScreenContainer.position.y < -1 || finScreenContainer.position.y > 2 * 10 * finInterval[1] + 1) {
        productScrollReady = false;
        
        
        stopDrag = true;
        let nextDirection = Math.ceil(finScreenContainer.position.y / (10 * finInterval[1])) * 10 * finInterval[1];
        if ((event.deltaY > 0 && finScreenContainer.position.y > 0) || finScreenContainer.position.y > (20 * finInterval[1])) {
          nextDirection = Math.floor(finScreenContainer.position.y / (10 * finInterval[1])) * 10 * finInterval[1];
        }
        finScreenPan = false;
        finScreenContainer.tween = gsap.to(finScreenContainer.position, { duration: 1 * Math.abs(nextDirection - finScreenContainer.position.y) / (10 * finInterval[1]), y: nextDirection, ease: "power2.out", onUpdate: function() {
         // animateFinScroll();
        }, onComplete: function() {
          if (finScreenContainer.tween !== null && finScreenContainer.tween !== undefined) {
            finScreenContainer.tween.kill();
            finScreenContainer.tween = null;
          }
          if (finScreenContainer.position.y == 0) {
            gsap.to(icon.scroll.scale, { duration: 0.3, x: 1, y: 1, ease: "power1.out" });
            gsap.to(icon.scroll.material, { duration: 0.3, opacity: 1, ease: "none" });
          
          }
          if (finScreenContainer.position.y == 20 * finInterval[1]) {
            finScrollCount++;
         
          }
          
          oldDeltaY = 0;
        } });
      }
    }
  }
  if (startScreenReady && !startScreenPan && event.eventType == 2 && !stopDrag) {
    startScreenPan = true;
    
    oldDeltaY = event.deltaY * (Math.tan(mainCamera.fov * Math.PI / 360) * 24 / window.innerHeight);
    if (startScreenContainer.tween !== null && startScreenContainer.tween !== undefined) {
      startScreenContainer.tween.kill();
      startScreenContainer.tween = null;
    }
    
    
    
  }
  
  
  if (userInput.ready && event.eventType == 2 && startScreenPan) {
  
  
  
    userInput.ready = false;
    if (button.goMovie.opacityTween != null && button.goMovie.opacityTween != undefined) {
      button.goMovie.opacityTween.kill();
      button.goMovie.opacityTween = null;
    }
    button.goMovie.opacityTween = gsap.to([button.goMovie.text.material, button.goMovie.border.material, button.goMovie.plane.material], { duration: 0.3, opacity: 0, ease: "none" });

 
  }
  
  if (startScreenPan && startScreenReady) {
    if (event.eventType == 2 && water.position.y > 0 && startScreenContainer.position.y < 30 * introInterval) {
      water.position.y = -1;
    }
    if (event.eventType == 2) {
    
    gsap.to(icon.scroll.scale, { duration: 0.3, x: 0, y: 0, ease: "power1.out" });
    gsap.to(icon.scroll.material, { duration: 0.3, opacity: 0, ease: "none" });
   // startScreenReady = false;
    
    }
    
    startScreenContainer.position.y += oldDeltaY - event.deltaY * (Math.tan(mainCamera.fov * Math.PI / 360) * 24 / window.innerHeight);
    
   // const screenOrder = Math.floor(startScreenContainer.position.y / (10 * introInterval));
   // const screenPos = startScreenContainer.position.y % (10 * introInterval);
    //animateGradient();
    oldDeltaY = event.deltaY * (Math.tan(mainCamera.fov * Math.PI / 360) * 24 / window.innerHeight);
    if (event.eventType == 4 || startScreenContainer.position.y < -1 || startScreenContainer.position.y > 4 * 10 * introInterval + 1) {
      stopDrag = true;
     
      let nextDirection = Math.ceil(startScreenContainer.position.y / (10 * introInterval)) * 10 * introInterval;
      if ((event.deltaY > 0 && startScreenContainer.position.y > 0) || startScreenContainer.position.y > (40 * introInterval)) {
        nextDirection = Math.floor(startScreenContainer.position.y / (10 * introInterval)) * 10 * introInterval;
      }
      startScreenPan = false;
      
      startScreenContainer.tween = gsap.to(startScreenContainer.position, { duration: 1.5 * Math.abs(nextDirection - startScreenContainer.position.y) / (10 * introInterval), y: nextDirection, ease: "power2.out", onUpdate: function() {
       // frontStartScreenContainer.position.y = startScreenContainer.position.y;
       // animateGradient();
      }, onComplete: function() {
        if (startScreenContainer.position.y <= 30 * introInterval) {
        gsap.to(icon.scroll.scale, { duration: 0.3, x: 1, y: 1, ease: "power1.out" });
        gsap.to(icon.scroll.material, { duration: 0.3, opacity: 1, ease: "none" });
          
        }
        if (startScreenContainer.tween !== null && startScreenContainer.tween !== undefined) {
         startScreenContainer.tween.kill();
         startScreenContainer.tween = null;
        }
        
        
        
        if (startScreenContainer.position.y == 40 * introInterval) {
          if (!onRainText) {
            rainTextReady = true;
            drawRainText(0);
            onRainText = true;
          }
          if (!checkIntroEndOnce) {
            checkIntroEndOnce = true;
          }
          if (button.goMovie.opacityTween != null && button.goMovie.opacityTween != undefined) {
            button.goMovie.opacityTween.kill();
            button.goMovie.opacityTween = null;
          }
          if (symbolCount > 0) {
            button.goMovieOpasity = 1;    
          } else {
            button.goMovieOpasity = 0.5;    
          
          }
          button.goMovie.opacityTween = gsap.to([button.goMovie.text.material, button.goMovie.border.material], { duration: 0.3, opacity: button.goMovieOpasity, ease: "none" });
          userInput.ready = true;
        } else if (startScreenContainer.position.y == 30 * introInterval && symbolCount == 0) {
          rainTextReady = false;
          onRainText = false;
          text.userText[1].line[0].geometry.dispose();
          text.userText[1].line[0].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(``, 0.28), 3);
          text.userText[1].line[0].geometry.computeBoundingBox();
          text.userText[1].line[0].geometry.translate(-text.userText[1].line[0].geometry.boundingBox.max.x / 2, 0, 0);
          text.userText[1].line[0].geometry.NeedUpdate = true;
          text.userText[0].line[0].geometry.dispose();
          text.userText[0].line[0].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(``, 0.28), 3);
          text.userText[0].line[0].geometry.computeBoundingBox();
          text.userText[0].line[0].geometry.translate(-text.userText[0].line[0].geometry.boundingBox.max.x / 2, 0, 0);
          text.userText[0].line[0].geometry.NeedUpdate = true;
          text.userText[0].line[1].geometry.dispose();
          text.userText[0].line[1].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(``, 0.28), 3);
          text.userText[0].line[1].geometry.computeBoundingBox();
          text.userText[0].line[1].geometry.translate(-text.userText[0].line[1].geometry.boundingBox.max.x / 2, 0, 0);
          text.userText[0].line[1].geometry.NeedUpdate = true;
        }
        water.position.y = 1;
        oldDeltaY = 0;
      } });
    }
  }
});

function onWheel(event) {
  if (!soundStarted) {
    document.getElementById("myVideo").play();
    soundStarted = true;
    goSound();
  }
  event.preventDefault();
  if (startScreenContainer.position.y >= 0 && startScreenContainer.position.y <= 40 * introInterval) {
    if (onIntro && startScreenPan && startScreenReady) {
      gsap.to(icon.scroll.scale, { duration: 0.3, x: 0, y: 0, ease: "power1.out" });
      gsap.to(icon.scroll.material, { duration: 0.3, opacity: 0, ease: "none" });
      userInput.ready = false;
      if (button.goMovie.opacityTween != null && button.goMovie.opacityTween != undefined) {
        button.goMovie.opacityTween.kill();
        button.goMovie.opacityTween = null;
      }
      button.goMovie.opacityTween = gsap.to([button.goMovie.text.material, button.goMovie.border.material, button.goMovie.plane.material], { duration: 0.3, opacity: 0, ease: "none" });
      startScreenPan = false;
      if (startScreenContainer.tween !== null && startScreenContainer.tween !== undefined) {
        startScreenContainer.tween.kill();
        startScreenContainer.tween = null;
      }
      let scrollTime = 1.5;
      let nextDirection = startScreenContainer.position.y + 10 * introInterval;
      if (event.deltaY < 0) {
        nextDirection = startScreenContainer.position.y - 10 * introInterval;
      }
      if (nextDirection < 0 || nextDirection > 40 * introInterval) {
        nextDirection = startScreenContainer.position.y;
        scrollTime = 0;
      }
      startScreenContainer.tween = gsap.to(startScreenContainer.position, { duration: scrollTime, y: nextDirection, ease: "power2.inOut", onComplete: function() {
        startScreenPan = true;
        if (startScreenContainer.position.y <= 30 * introInterval) {
          gsap.to(icon.scroll.scale, { duration: 0.3, x: 1, y: 1, ease: "power1.out" });
          gsap.to(icon.scroll.material, { duration: 0.3, opacity: 1, ease: "none" });
        }
        if (startScreenContainer.tween !== null && startScreenContainer.tween !== undefined) {
         startScreenContainer.tween.kill();
         startScreenContainer.tween = null;
        }
        if (startScreenContainer.position.y == 40 * introInterval) {
          if (!onRainText) {
            rainTextReady = true;
            drawRainText(0);
            onRainText = true;
          }
          if (!checkIntroEndOnce) {
            checkIntroEndOnce = true;
          }
          if (button.goMovie.opacityTween != null && button.goMovie.opacityTween != undefined) {
            button.goMovie.opacityTween.kill();
            button.goMovie.opacityTween = null;
          }
          if (symbolCount > 0) {
            button.goMovieOpasity = 1;    
          } else {
            button.goMovieOpasity = 0.5;    
          }
          button.goMovie.opacityTween = gsap.to([button.goMovie.text.material, button.goMovie.border.material, button.goMovie.plane.material], { duration: 0.3, opacity: button.goMovieOpasity, ease: "none" });
          userInput.ready = true;
        } else if (startScreenContainer.position.y == 30 * introInterval && symbolCount == 0) {
          rainTextReady = false;
          onRainText = false;
          text.userText[1].line[0].geometry.dispose();
          text.userText[1].line[0].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(``, 0.28), 3);
          text.userText[1].line[0].geometry.computeBoundingBox();
          text.userText[1].line[0].geometry.translate(-text.userText[1].line[0].geometry.boundingBox.max.x / 2, 0, 0);
          text.userText[1].line[0].geometry.NeedUpdate = true;
          text.userText[0].line[0].geometry.dispose();
          text.userText[0].line[0].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(``, 0.28), 3);
          text.userText[0].line[0].geometry.computeBoundingBox();
          text.userText[0].line[0].geometry.translate(-text.userText[0].line[0].geometry.boundingBox.max.x / 2, 0, 0);
          text.userText[0].line[0].geometry.NeedUpdate = true;
          text.userText[0].line[1].geometry.dispose();
          text.userText[0].line[1].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(``, 0.28), 3);
          text.userText[0].line[1].geometry.computeBoundingBox();
          text.userText[0].line[1].geometry.translate(-text.userText[0].line[1].geometry.boundingBox.max.x / 2, 0, 0);
          text.userText[0].line[1].geometry.NeedUpdate = true;
        }
          water.position.y = 1;
      } });
    }
  }
  if (finScreenContainer.position.y >= 0 && finScreenContainer.position.y <= 20 * finInterval[1]) {
    if (onFin && finScreenPan && finScreenReady) {
      if (finScreenContainer.position.y == 0) {
        gsap.to(icon.scroll.scale, { duration: 0.3, x: 0, y: 0, ease: "power1.out" });
        gsap.to(icon.scroll.material, { duration: 0.3, opacity: 0, ease: "none" });
      }
      let scrollTime = 1.5;
      let nextDirection = finScreenContainer.position.y + 10 * finInterval[1];
      if (event.deltaY < 0) {
        nextDirection = finScreenContainer.position.y - 10 * finInterval[1];
        }
      if (nextDirection < 0 || nextDirection > 20 * finInterval[1]) {
        nextDirection = finScreenContainer.position.y;
        scrollTime = 0;
      }
      finScreenPan = false;
      finScreenContainer.tween = gsap.to(finScreenContainer.position, { duration: scrollTime, y: nextDirection, ease: "power2.inOut", onComplete: function() {
        finScreenPan = true;
        if (finScreenContainer.position.y == 0) {
          gsap.to(icon.scroll.scale, { duration: 0.3, x: 1, y: 1, ease: "power1.out" });
          gsap.to(icon.scroll.material, { duration: 0.3, opacity: 1, ease: "none" });
        }
        if (screenStat == 0) {
          if (finScreenContainer.position.y == 30 * finInterval[0]) {
            finScrollCount++;
          }
          if (finScreenContainer.position.y == 40 * finInterval[0] && finScrollCount == 2) {
            finScrollCount--;
          }
        } else {
          if (finScreenContainer.position.y == 20 * finInterval[1]) {
            finScrollCount++;
            
          }
        }
        if (finScreenContainer.tween !== null && finScreenContainer.tween !== undefined) {
          finScreenContainer.tween.kill();
          finScreenContainer.tween = null;
        }
      } });
    }
  }
}





function goInput() {
  document.addEventListener( 'keypress', onDocumentKeyPress );
  document.addEventListener( 'keydown', onDocumentKeyDown );

  userInput.ready = false;
  startScreenReady = false;
 // if (!isMobile()) {
    if (button.goMovie.opacityTween != null && button.goMovie.opacityTween != undefined) {
      button.goMovie.opacityTween.kill();
      button.goMovie.opacityTween = null;
    }
    gsap.to([button.goMovie.text.material, button.goMovie.border.material, button.goMovie.plane.material], { duration: 0.2, opacity: 0, ease: "none" });
 // }
  
  showKeyboard();
 
    
  
}

let keyboardReady = false;


function showKeyboard() {
  if (symbolCount == 0) {
    rainTextReady = false;
    clearUserText();
    onRainText = false;
  }
  gsap.to([text.userText[0].cursor.scale, text.userText[1].cursor.scale], { duration: 0.3, x: 1, y: 1, ease: "back.out" });
  text.userText[1].line[0].material.color.setHex(0x000000);
  text.userText[0].line[0].material.color.setHex(0x000000);
  text.userText[0].line[1].material.color.setHex(0x000000);
  text.userText[0].line[2].material.color.setHex(0x000000);
  text.hideStep = 1;
  if (text.hideTween != null && text.hideTween != undefined) {
    text.hideTween.kill();
    text.hideTween = null;
  }
  text.hideTween = gsap.to(text, { duration: 0.4, hideStep: 0, ease: "none", onUpdate: function() {
    showSeparatedText(text.inputText[0], text.hideStep);
    showSeparatedText(text.inputText[1], text.hideStep);

  } });
  
  gsap.to(text.count.material, { duration: 0.5, opacity: 1, ease: "none" });
  if (isMobile()) {
    gsap.to([userInput.position, userInput.field[0].position, userInput.field[1].position], { duration: 0.5, y: userInput.position.y + 1.5, ease: "power1.inOut" });
  
    gsap.to(keyboard.position, { duration: 0.6, y: 2.3, ease: "power2.out", onComplete: function() {
      keyboard.ready = true;
    } });
  } else {
    gsap.to(keyboard.position, { duration: 0.6, y: keyboard.position.y, ease: "power2.out", onComplete: function() {

      keyboardReady = true;
    } });
  }
}

function onDocumentKeyPress( event ) {
  if (keyboardReady) {
    const keyCode = event.which;
    if (keyCode == 8) {
      event.preventDefault();
    } else {
    //  alert("press")
      
      const symbol = String.fromCharCode( keyCode );
      if (!(inputSymbols[inputSymbols.length - 1] == ` ` && symbol == ` `) && symbolCount < 51) {

        goType(symbol);
      }
    }
  }
}
function onDocumentKeyDown( event ) {
  if (keyboardReady) {
    const keyCode = event.which;
  if ( keyCode == 8 ) {
    event.preventDefault();
    goBackSpace();
  }
  if (keyCode == 13) {
    keyboardReady = false;
    event.preventDefault();
    hideKeyboard();
  }
  
  }
}

let rainTextReady = true;
let onRainText = false;
function drawRainText(step) {
  if (step <= waterText[7].length) {
    text.userText[0].line[0].geometry.dispose();
    text.userText[0].line[0].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(waterText[7].slice(0, step), 0.28), 3);
    text.userText[0].line[0].geometry.computeBoundingBox();
    text.userText[0].line[0].geometry.translate(-text.userText[0].line[0].geometry.boundingBox.max.x / 2, 0, 0);
    text.userText[0].line[0].geometry.NeedUpdate = true;
  } else if (step <= waterText[7].length + waterText[8].length) {
    text.userText[0].line[1].geometry.dispose();
    text.userText[0].line[1].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(waterText[8].slice(0, step - waterText[7].length), 0.28), 3);
    text.userText[0].line[1].geometry.computeBoundingBox();
    text.userText[0].line[1].geometry.translate(-text.userText[0].line[1].geometry.boundingBox.max.x / 2, 0, 0);
    text.userText[0].line[1].geometry.NeedUpdate = true;
  }
  text.userText[1].line[0].geometry.dispose();
  text.userText[1].line[0].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(waterText[9].slice(0, step), 0.28), 3);
  text.userText[1].line[0].geometry.computeBoundingBox();
  text.userText[1].line[0].geometry.translate(-text.userText[1].line[0].geometry.boundingBox.max.x / 2, 0, 0);
  text.userText[1].line[0].geometry.NeedUpdate = true;
  if (step > waterText[9].length) rainTextReady = false;
  if (rainTextReady) {
    setTimeout(function() {
      if (rainTextReady) {
        step ++;
        drawRainText(step);
      }
    }, 40);
  }
}


function hideKeyboard() {
  gsap.to([text.userText[0].cursor.scale, text.userText[1].cursor.scale], { duration: 0.3, x: 0, y: 0, ease: "back.in" });
  if (symbolCount == 0) {
   
       
    
    text.userText[0].line[0].material.color.setHex(0xe0e0e0);
    text.userText[1].line[0].material.color.setHex(0xe0e0e0);
    text.userText[0].line[1].material.color.setHex(0xe0e0e0);
    text.userText[0].line[0].position.y = 0.87;
    text.userText[0].line[1].position.y = 0.35;
    rainTextReady = true;
    drawRainText(0);
    onRainText = true;
    
    
    shape = null;
    shape = new THREE.Shape();
    shape.absarc(-2.8, 0, 0.75, Math.PI * 0.5, Math.PI * 1.5);
    shape.absarc(2.8, 0, 0.75, Math.PI * 1.5, Math.PI * 2.5);
    userInput.field[0].geometry.dispose();
    userInput.field[0].geometry = new THREE.ShapeBufferGeometry(shape, 36);
    userInput.field[0].geometry.NeedUpdate = true;
    userInput.field[0].geometry.translate(0, 0.75, 0);

  }
  
  
  text.hideStep = 0;
  
  if (text.hideTween != null && text.hideTween != undefined) {
    text.hideTween.kill();
    text.hideTween = null;
  }
  text.hideTween = gsap.to(text, { duration: 0.4, hideStep: 1, ease: "none", onUpdate: function() {
    showSeparatedText(text.inputText[0], text.hideStep);
    showSeparatedText(text.inputText[1], text.hideStep);

  } });
  if (isMobile()) {
    gsap.to([userInput.position, userInput.field[0].position, userInput.field[1].position], { duration: 0.4, y: userInput.position.y - 1.5, ease: "power1.inOut" });
  }

  gsap.to(text.count.material, { duration: 0.4, opacity: 0, ease: "none" });

  gsap.to(keyboard.position, { duration: 0.4, y: -4, ease: "power2.in", onComplete: function() {
    
    
    if (symbolCount > 0) {
      button.goMovieOpasity = 1;
      
      
      button.goMovie.border.material.color.setHex(0x000000);
      button.goMovie.text.material.color.setHex(0x000000);

    } else {
      button.goMovieOpasity = 0.5;    
      
      button.goMovie.plane.visible = false;
      button.goMovie.border.material.color.setHex(0xffffff);
      button.goMovie.text.material.color.setHex(0xffffff);

    }
    gsap.to([button.goMovie.text.material, button.goMovie.border.material], { duration: 0.2, opacity: button.goMovieOpasity, ease: "none", onComplete: function() {
      if (symbolCount > 0) {
        button.goMovie.plane.visible = true;
      }
      userInput.ready = true;
      startScreenReady = true;
      onIntro = true;
    } });

  } });
}

let inputSymbols = ``;
let space = 0;
let symbolCount = 0;
function goType(symbol) {
    
  if (!(inputSymbols[inputSymbols.length - 1] == ` ` && keyboardData[symbol] == ` `) && symbolCount < 51) {
    symbolCount ++;
    changeCount();
    
    if (isMobile()) {
      if (keyboard.upperCase) {
        inputSymbols += keyboardData[symbol].toUpperCase();
      } else {
        inputSymbols += keyboardData[symbol];
      }
    } else {
      inputSymbols += symbol;
    }
    if (inputSymbols[inputSymbols.length - 1] == ` `) {
      space = 0.15;
    } else {
      space = 0;
    }
   // text.userText[1].line[0].geometry.computeBoundingBox();
    text.userText[1].line[0].geometry.dispose();
    text.userText[1].line[0].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(inputSymbols, 0.28), 3);
    text.userText[1].line[0].geometry.computeBoundingBox();
    text.userText[1].cursor.position.x = text.userText[1].line[0].geometry.boundingBox.max.x / 2 + 0.05 + space;

    text.userText[1].line[0].geometry.translate(-text.userText[1].line[0].geometry.boundingBox.max.x / 2, 0, 0);
    
    text.userText[1].line[0].geometry.NeedUpdate = true;
 
    
    
    
    
    text.userText[0].line[1].geometry.computeBoundingBox();
    text.userText[0].line[2].geometry.computeBoundingBox();
    if (text.userText[0].line[2].geometry.boundingBox.max.x > 0) {
      text.userText[0].line[2].geometry.dispose();
    text.userText[0].line[2].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(inputSymbols.slice(text.userText[0].wrapPoint1, inputSymbols.length), 0.28), 3);
    text.userText[0].line[2].geometry.computeBoundingBox();
    text.userText[0].cursor.position.x = text.userText[0].line[2].geometry.boundingBox.max.x / 2 + 0.05 + space;

    text.userText[0].line[2].geometry.translate(-text.userText[0].line[2].geometry.boundingBox.max.x / 2, 0, 0);
    
    text.userText[0].line[2].geometry.NeedUpdate = true;
    
    
  } else if (text.userText[0].line[1].geometry.boundingBox.max.x > 0) {
    text.userText[0].line[1].geometry.dispose();
    text.userText[0].line[1].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(inputSymbols.slice(text.userText[0].wrapPoint, inputSymbols.length), 0.28), 3);
    text.userText[0].line[1].geometry.computeBoundingBox();
   // text.userText[0].line[1].geometry.computeBoundingBox();
   // text.userText[0].line[1].geometry.translate(-text.userText[0].line[1].geometry.boundingBox.max.x / 2, 0, 0);
    //text.userText[0].line[1].geometry.NeedUpdate = true;    

    if (text.userText[0].line[1].geometry.boundingBox.max.x > 6.4) {
      for (let i = inputSymbols.length; i >= text.userText[0].wrapPoint; i--) {
        if (inputSymbols[i] == ` `) {
          
          text.userText[0].wrapPoint1 = i + 1;
          i = -1;
        }
        if (i == text.userText[0].wrapPoint) {
          
          text.userText[0].wrapPoint1 = inputSymbols.length - 1;
        }
      }
      text.userText[0].line[1].geometry.dispose();
      text.userText[0].line[1].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(inputSymbols.slice(text.userText[0].wrapPoint, text.userText[0].wrapPoint1), 0.28), 3);
      
      
      text.userText[0].line[2].geometry.dispose();
      text.userText[0].line[2].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(inputSymbols.slice(text.userText[0].wrapPoint1, inputSymbols.length), 0.28), 3);
      text.userText[0].line[2].geometry.computeBoundingBox();
      text.userText[0].line[2].geometry.translate(-text.userText[0].line[2].geometry.boundingBox.max.x / 2, 0, 0);
      text.userText[0].line[2].geometry.NeedUpdate = true;
      text.userText[0].cursor.position.x = text.userText[0].line[2].geometry.boundingBox.max.x + 0.05 + space;
     
     
      text.userText[0].line[0].position.y += 0.52;
      text.userText[0].line[1].position.y += 0.52;
      shape = null;
      shape = new THREE.Shape();
      shape.absarc(-2.45, 0, 1.05, Math.PI * 0.5, Math.PI * 1.5);
      shape.absarc(2.45, 0, 1.05, Math.PI * 1.5, Math.PI * 2.5);

      userInput.field[0].geometry.dispose();
 
      userInput.field[0].geometry = new THREE.ShapeBufferGeometry(shape, 36);
      userInput.field[0].geometry.NeedUpdate = true;

 
      userInput.field[0].geometry.translate(0, 1.05, 0);

    } else {
      text.userText[0].cursor.position.x = text.userText[0].line[1].geometry.boundingBox.max.x / 2 + 0.05 + space;

    }
    
    text.userText[0].line[1].geometry.computeBoundingBox();
    text.userText[0].line[1].geometry.translate(-text.userText[0].line[1].geometry.boundingBox.max.x / 2, 0, 0);
    text.userText[0].line[1].geometry.NeedUpdate = true;    

  } else {
    text.userText[0].line[0].geometry.dispose();
    text.userText[0].line[0].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(inputSymbols, 0.28), 3);
    text.userText[0].line[0].geometry.computeBoundingBox();
    if (text.userText[0].line[0].geometry.boundingBox.max.x > 6.4) {
      for (let i = inputSymbols.length; i >= 0; i--) {
        if (inputSymbols[i] == ` `) {
          text.userText[0].wrapPoint = i + 1;
          i = -1;
        }
        if (i == 0) {
         
          text.userText[0].wrapPoint = inputSymbols.length - 1;
        }
      }
      text.userText[0].line[0].geometry.dispose();
      text.userText[0].line[0].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(inputSymbols.slice(0, text.userText[0].wrapPoint), 0.28), 3);
      text.userText[0].line[1].geometry.dispose();
      text.userText[0].line[1].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(inputSymbols.slice(text.userText[0].wrapPoint, inputSymbols.length), 0.28), 3);
      text.userText[0].line[1].geometry.NeedUpdate = true;
      text.userText[0].line[1].geometry.computeBoundingBox();
      text.userText[0].line[1].geometry.translate(-text.userText[0].line[1].geometry.boundingBox.max.x / 2, 0, 0);
      text.userText[0].cursor.position.x = text.userText[0].line[1].geometry.boundingBox.max.x + 0.05 + space;
      
      text.userText[0].line[0].position.y += 0.52;
      shape = null;
      shape = new THREE.Shape();
      shape.absarc(-2.8, 0, 0.75, Math.PI * 0.5, Math.PI * 1.5);
      shape.absarc(2.8, 0, 0.75, Math.PI * 1.5, Math.PI * 2.5);

      userInput.field[0].geometry.dispose();
 
      userInput.field[0].geometry = new THREE.ShapeBufferGeometry(shape, 36);
      userInput.field[0].geometry.NeedUpdate = true;

 
      userInput.field[0].geometry.translate(0, 0.75, 0);

    } else {
      text.userText[0].cursor.position.x = text.userText[0].line[0].geometry.boundingBox.max.x / 2 + 0.05 + space;
    }
    text.userText[0].line[0].geometry.computeBoundingBox();
    text.userText[0].line[0].geometry.translate(-text.userText[0].line[0].geometry.boundingBox.max.x / 2, 0, 0);
    text.userText[0].line[0].geometry.NeedUpdate = true;
  } 
  if (keyboard.upperCase && !keyboard.UpperCaseStatic) {
    keyboard.upperCase = false;
    for (let i = 0; i < 76; i++) {
      keyboard.button[i].symbol.geometry.dispose();
      keyboard.button[i].symbol.geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(keyboardData[i], 0.8), 3);
      keyboard.button[i].symbol.geometry.computeBoundingBox();
      keyboard.button[i].symbol.geometry.translate(-keyboard.button[i].symbol.geometry.boundingBox.max.x / 2, -0.4, 0);
      keyboard.button[i].symbol.geometry.NeedUpdate = true;
    }
  }
  }
  
}
function goBackSpace() {
  if (inputSymbols.length > 0) {
    symbolCount --;
    changeCount();
  }
  
  inputSymbols = inputSymbols.substring(0, inputSymbols.length - 1);
  if (inputSymbols[inputSymbols.length - 1] == ` `) {
    space = 0.15;
  } else {
    space = 0;
  }

  text.userText[0].line[0].geometry.computeBoundingBox();
  
  text.userText[0].line[1].geometry.computeBoundingBox();
  text.userText[0].line[2].geometry.computeBoundingBox();

  if (text.userText[0].line[2].geometry.boundingBox.max.x > 0) {
    text.userText[0].line[2].geometry.dispose();
    text.userText[0].line[2].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(inputSymbols.slice(text.userText[0].wrapPoint1, inputSymbols.length), 0.28), 3);
    text.userText[0].line[2].geometry.computeBoundingBox();
    if (text.userText[0].line[2].geometry.boundingBox.max.x + text.userText[0].line[1].geometry.boundingBox.max.x * 2 + 0.3 < 6.4) {
      text.userText[0].line[1].geometry.dispose();
      text.userText[0].line[1].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(inputSymbols.slice(text.userText[0].wrapPoint, inputSymbols.length), 0.28), 3);
      text.userText[0].line[1].geometry.computeBoundingBox();
      text.userText[0].line[1].geometry.translate(-text.userText[0].line[1].geometry.boundingBox.max.x / 2, 0, 0);
      text.userText[0].line[1].geometry.NeedUpdate = true;    

      text.userText[0].line[2].geometry.dispose();
      text.userText[0].line[2].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(``, 0.28), 3);
      text.userText[0].line[2].geometry.computeBoundingBox();
      text.userText[0].line[0].position.y -= 0.52;
      text.userText[0].line[1].position.y -= 0.52;
      text.userText[0].cursor.position.x = text.userText[0].line[1].geometry.boundingBox.max.x + 0.05 + space;
      shape = null;
      shape = new THREE.Shape();
      shape.absarc(-2.8, 0, 0.75, Math.PI * 0.5, Math.PI * 1.5);
      shape.absarc(2.8, 0, 0.75, Math.PI * 1.5, Math.PI * 2.5);

      userInput.field[0].geometry.dispose();
 
      userInput.field[0].geometry = new THREE.ShapeBufferGeometry(shape, 36);
      userInput.field[0].geometry.NeedUpdate = true;

 
      userInput.field[0].geometry.translate(0, 0.75, 0);

    } else {
      text.userText[0].cursor.position.x = text.userText[0].line[2].geometry.boundingBox.max.x / 2 + 0.05 + space;
    }
    text.userText[0].line[2].geometry.translate(-text.userText[0].line[2].geometry.boundingBox.max.x / 2, 0, 0);
    text.userText[0].line[2].geometry.NeedUpdate = true;

  } else if (text.userText[0].line[1].geometry.boundingBox.max.x > 0) {
    text.userText[0].line[1].geometry.dispose();
    text.userText[0].line[1].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(inputSymbols.slice(text.userText[0].wrapPoint, inputSymbols.length), 0.28), 3);
    text.userText[0].line[1].geometry.computeBoundingBox();
    if (text.userText[0].line[1].geometry.boundingBox.max.x + text.userText[0].line[0].geometry.boundingBox.max.x * 2 + 0.3 < 6.4) {
      text.userText[0].line[0].geometry.dispose();
      text.userText[0].line[0].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(inputSymbols.slice(0, inputSymbols.length), 0.28), 3);
      text.userText[0].line[0].geometry.computeBoundingBox();
      text.userText[0].line[0].geometry.translate(-text.userText[0].line[0].geometry.boundingBox.max.x / 2, 0, 0);
      text.userText[0].line[0].geometry.NeedUpdate = true;    

      text.userText[0].line[1].geometry.dispose();
      text.userText[0].line[1].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(``, 0.28), 3);
      text.userText[0].line[1].geometry.computeBoundingBox();
      //alert(text.userText[0].line[0].geometry.boundingBox.max.x)
      text.userText[0].cursor.position.x = text.userText[0].line[0].geometry.boundingBox.max.x + 0.05 + space;
    //  text.userText[0].cursor.position.x = 4
      text.userText[0].line[0].position.y -= 0.52;
      shape = null;
      shape = new THREE.Shape();
      shape.absarc(-3.05, 0, 0.5, Math.PI * 0.5, Math.PI * 1.5);
      shape.absarc(3.05, 0, 0.5, Math.PI * 1.5, Math.PI * 2.5);
      userInput.field[0].geometry.dispose();
 
      userInput.field[0].geometry = new THREE.ShapeBufferGeometry(shape, 36);
      userInput.field[0].geometry.NeedUpdate = true;

 
      userInput.field[0].geometry.translate(0, 0.5, 0);

    } else {
      text.userText[0].cursor.position.x = text.userText[0].line[1].geometry.boundingBox.max.x / 2 + 0.05 + space;
    }
    text.userText[0].line[1].geometry.translate(-text.userText[0].line[1].geometry.boundingBox.max.x / 2, 0, 0);
    text.userText[0].line[1].geometry.NeedUpdate = true;

  } else if (text.userText[0].line[0].geometry.boundingBox.max.x > 0) {
    text.userText[0].line[0].geometry.dispose();
    text.userText[0].line[0].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(inputSymbols, 0.28), 3);
    text.userText[0].line[0].geometry.computeBoundingBox();
    text.userText[0].line[0].geometry.translate(-text.userText[0].line[0].geometry.boundingBox.max.x / 2, 0, 0);
    text.userText[0].line[0].geometry.NeedUpdate = true;
    if (text.userText[0].line[0].geometry.boundingBox.max.x > 0) {
      text.userText[0].cursor.position.x = text.userText[0].line[0].geometry.boundingBox.max.x + 0.05 + space;
    } else {
      text.userText[0].cursor.position.x = 0;
    }
    
  }
  text.userText[1].line[0].geometry.dispose();
  text.userText[1].line[0].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(inputSymbols, 0.28), 3);
  text.userText[1].line[0].geometry.computeBoundingBox();
  text.userText[1].line[0].geometry.translate(-text.userText[1].line[0].geometry.boundingBox.max.x / 2, 0, 0);
  text.userText[1].line[0].geometry.NeedUpdate = true;
  if (text.userText[1].line[0].geometry.boundingBox.max.x > 0) {
    text.userText[1].cursor.position.x = text.userText[1].line[0].geometry.boundingBox.max.x + 0.05 + space;
  } else {
    text.userText[1].cursor.position.x = 0;
  }

}

function changeCount() {
  text.count.geometry.dispose();
  text.count.geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(`` + symbolCount + `/50`, 0.18), 3);
  text.count.geometry.computeBoundingBox();
  text.count.geometry.translate(-text.count.geometry.boundingBox.max.x + text.count.posY, 0, 0);
  text.count.geometry.NeedUpdate = true;
  if (symbolCount == 51) {
    text.count.material.color.setHex(0xff5353);
  } else {
    text.count.material.color.setHex(0xa6a6a6);
  }
}

function clearUserText() {
  text.userText[1].line[0].geometry.dispose();
  text.userText[1].line[0].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes('', 0.28), 3);
  text.userText[1].line[0].geometry.NeedUpdate = true;
  text.userText[0].line[0].geometry.dispose();
  text.userText[0].line[0].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes('', 0.28), 3);
  text.userText[0].line[0].geometry.NeedUpdate = true;
  text.userText[0].line[1].geometry.dispose();
  text.userText[0].line[1].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes('', 0.28), 3);
  text.userText[0].line[1].geometry.NeedUpdate = true;
  text.userText[0].line[2].geometry.dispose();
  text.userText[0].line[2].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes('', 0.28), 3);
  text.userText[0].line[2].geometry.NeedUpdate = true;
  text.userText[0].line[0].position.set(0, 0.35, 0);
  text.userText[0].cursor.position.x = 0;
  text.userText[1].cursor.position.x = 0;
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-3.05, 0, 0.5, Math.PI * 0.5, Math.PI * 1.5);
  shape.absarc(3.05, 0, 0.5, Math.PI * 1.5, Math.PI * 2.5);
  userInput.field[0].geometry.dispose()
 
  userInput.field[0].geometry = new THREE.ShapeBufferGeometry(shape, 36);
  userInput.field[0].geometry.NeedUpdate = true;

 
  userInput.field[0].geometry.translate(0, 0.5, 0);

  
}
const whiteColor = new THREE.Color(0xffffff);
let onMovie = false;
function startMovie() {
  text.hideStep = 1;
  breakSymbols();
  
  
  gsap.to([svgPic[0].material.color, pic[6].color], { duration: 1, r: whiteColor.r, g: whiteColor.g, b: whiteColor.b, ease: "none" });

  for (let i = 0; i < text.userText[0].symbol.length; i++) {
    const time = 1 + Math.random() * 2;
    gsap.to(text.userText[0].symbol[i].position, { duration: time, x: text.userText[0].symbol[i].position.x - 2 + Math.random() * 4, y: text.userText[0].symbol[i].position.y - 2 + Math.random() * 4, ease: "power1.inOut" });
    gsap.to(text.userText[0].symbol[i].material, { duration: time, opacity: 0, ease: "power2.out" });
    gsap.to(text.userText[0].symbol[i].scale, { duration: time, x: 0, y: 0, ease: "power1.inOut" });
  }
  for (let i = 0; i < text.userText[1].symbol.length; i++) {
    const time = 1 + Math.random() * 2;
    gsap.to(text.userText[1].symbol[i].position, { duration: time, x: text.userText[1].symbol[i].position.x - 2 + Math.random() * 4, y: text.userText[1].symbol[i].position.y - 2 + Math.random() * 4, ease: "power1.inOut" });
    gsap.to(text.userText[1].symbol[i].material, { duration: time, opacity: 0, ease: "power2.out" });
    gsap.to(text.userText[1].symbol[i].scale, { duration: time, x: 0, y: 0, ease: "power1.inOut" });
  }
  


  gsap.to(text, { duration: 0.7, hideStep: 0, ease: "none", onUpdate: function() {
    showSeparatedText(text.inputText[0], text.hideStep);
    showSeparatedText(text.inputText[1], text.hideStep);
  } });
  gsap.to([userInput.field[0].material, userInput.field[1].material], { duration: 0.2, opacity: 0, ease: "none" });
  gsap.to(button.goMovie.scale, { duration: 0.2, x: 0.9, y: 0.9, ease: "power1.out", onComplete: function() {
    gsap.to([button.goMovie.text.material, button.goMovie.border.material, button.goMovie.plane.material], { duration: 0.4, opacity: 0, ease: "none" });
    water.position.y = -3;
    water.tween = gsap.to(water.position, { duration: 7, y: -11, ease: "power1.in" });
    
    for (let i = 0; i < text.userText[0].newLine.count; i++) {
      text.userText[0].newLine[i].position.y = (text.userText[0].newLine.count - 1) / 2 * 0.6 - 0.25 - 0.6 * i;
      const random = 1.5 + Math.random() * 2;
      gsap.from(text.userText[0].newLine[i].position, { duration: random, x: -1 + Math.random() * 2, ease: "power1.inOut" });
      gsap.from(text.userText[0].newLine[i].scale, { duration: random, x: 0, y: 0, ease: "power1.out" });
    }
    for (let i = 0; i < text.userText[1].newLine.count; i++) {
      text.userText[1].newLine[i].position.y = (text.userText[1].newLine.count - 1) / 2 * 0.6 - 0.25 - 0.6 * i;
      const random = 1.5 + Math.random() * 2;
      gsap.from(text.userText[1].newLine[i].position, { duration: random, x: -1 + Math.random() * 2, ease: "power1.inOut" });
      gsap.from(text.userText[1].newLine[i].scale, { duration: random, x: 0, y: 0, ease: "power1.out" });
    }
    onMovie = true;
    text.userText[screenStat].container.visible = true;
        if (blurTween !== null && blurTween !== undefined) {
      blurTween.kill();
      blurTween = null;
    }
    vblur.uniforms.v.value = 0;
    hblur.uniforms.h.value = 0;
    gsap.to(button.speed.scale, { duration: 0.3, x: 1, y: 1, ease: "power1.out", delay: 1 });
    gsap.to([button.speed.icon_1.material, button.speed.icon_2.material], { duration: 0.3, opacity: 1, ease: "power1.out", delay: 1 });


    blurTween = gsap.to(screenBlur, { duration: 30, value: 0.5, ease: "power4.out", onUpdate: function() {
      vblur.uniforms.v.value = screenBlur.value / window.innerHeight * window.devicePixelRatio;
      hblur.uniforms.h.value = screenBlur.value / window.innerWidth * window.devicePixelRatio;
    }, onComplete: function() {
      
      let positionAttribute = [];
      
      for (let k = 0; k < text.userText[0].newLine.count; k++) {
        positionAttribute[k] = text.userText[0].newLine[k].geometry.getAttribute('position');
        const vertex = [];
        for (let i = 0; i < positionAttribute[k].count; i += 3) {
          const  timeDelay = Math.random() * 4;
          const num = i
          let vertex = [new THREE.Vector3(), new THREE.Vector3(), new THREE.Vector3()];
          for (let j = 0; j < 3; j ++) {
            vertex[j].fromBufferAttribute(positionAttribute[k], i + j); // read vertex
          }
          const time = {t: 1};
          if (particleTween[k][i] !== null && particleTween[k][i] !== undefined) {
            particleTween[k][i].kill();
            particleTween[k][i] = null;
          }
        
          gsap.to(time, { duration: 2, t: 0, ease: "power1.in", delay: timeDelay, onUpdate: function() {
            text.userText[0].newLine[k].geometry.attributes.position.setXYZ(num, vertex[2].x + (vertex[0].x - vertex[2].x) * time.t, vertex[2].y + (vertex[0].y - vertex[2].y) * time.t, vertex[0].z); // write coordinates back
            text.userText[0].newLine[k].geometry.attributes.position.setXYZ(num + 1, vertex[2].x + (vertex[1].x - vertex[2].x) * time.t, vertex[2].y + (vertex[1].y - vertex[2].y) * time.t, vertex[1].z); // write coordinates back
            text.userText[0].newLine[k].geometry.attributes.position.needsUpdate = true;
          } });
        }
      }
      


      let positionAttribute1 = [];
      
      for (let k = 0; k < text.userText[1].newLine.count; k++) {
        positionAttribute1[k] = text.userText[1].newLine[k].geometry.getAttribute('position');
        const vertex = [];
        for (let i = 0; i < positionAttribute1[k].count; i += 3) {
          const  timeDelay = Math.random() * 4;
          const num = i
          let vertex = [new THREE.Vector3(), new THREE.Vector3(), new THREE.Vector3()];
          for (let j = 0; j < 3; j ++) {
            vertex[j].fromBufferAttribute(positionAttribute1[k], i + j); // read vertex
          }
          const time = {t: 1};
          if (particleTween1[k][i] !== null && particleTween1[k][i] !== undefined) {
            particleTween1[k][i].kill();
            particleTween1[k][i] = null;
          }
        
          gsap.to(time, { duration: 2, t: 0, ease: "power1.in", delay: timeDelay, onUpdate: function() {
            text.userText[1].newLine[k].geometry.attributes.position.setXYZ(num, vertex[2].x + (vertex[0].x - vertex[2].x) * time.t, vertex[2].y + (vertex[0].y - vertex[2].y) * time.t, vertex[0].z); // write coordinates back
            text.userText[1].newLine[k].geometry.attributes.position.setXYZ(num + 1, vertex[2].x + (vertex[1].x - vertex[2].x) * time.t, vertex[2].y + (vertex[1].y - vertex[2].y) * time.t, vertex[1].z); // write coordinates back
            text.userText[1].newLine[k].geometry.attributes.position.needsUpdate = true;
          } });
        }
      }

      /*if (blurTween !== null && blurTween !== undefined) {
        blurTween.kill();
        blurTween = null;
      }
      gsap.to([button.speed.icon_1.material, button.speed.icon_2.material], { duration: 0.5, opacity: 0, ease: "none" });
      gsap.to(button.speed.scale, { duration: 2, x: 0, y: 0, ease: "power1.out" });

      blurTween = gsap.to(screenBlur, { duration: 6, value: 0.5, ease: "none", onComplete: function() {
        goBlur = false;
        
        goFin();
      } });*/
    } });
  } });
  text.gradStep = 1;
  gsap.to(text, { duration: 4, gradStep: 0, ease: "powerq.inOut", onUpdate: function() {
    document.getElementById('gradient').style.background = `linear-gradient(rgba(255, 255, 255, ${text.gradStep * 0.95}) 35%, rgba(48, 174, 191, ${0.8 - 0.8 * (1 - text.gradStep)}))`;
  } });
} 
const blackColor = new THREE.Color(0x000000);
let onFin = false;
function goFin() {
  gsap.to([svgPic[0].material.color, pic[6].color], { duration: 1, r: blackColor.r, g: blackColor.g, b: blackColor.b, ease: "none" });
  waterCenterContainer.remove(startScreenContainer);
  frontCenterContainer.remove(frontStartScreenContainer);
  water.position.y = 1;
  onFinScreen = true;
  onIntroScreen = false;
  text.gradStep = 0;
  gsap.to(text, { duration: 3, gradStep: 1, ease: "power1.inOut", onUpdate: function() {
    document.getElementById('gradient').style.background = `linear-gradient(0deg, rgba(255, 255, 255, ${text.gradStep * 0.95}) 65%, rgba(48, 174, 191, ${0.8 * text.gradStep}))`;
  } });
  text.finText[0].object.position.y = text.finText[0].posY[screenStat];
  const showFinTimer = { step: 0 };
  gsap.to(showFinTimer, { duration: 1.5, step: 1, ease: "power1.out", onUpdate: function() {
    showSeparatedText(text.finText[0], showFinTimer.step);
  }, onComplete: function() {
    text.finText[1].object.position.y = text.finText[1].posY[screenStat];
    showFinTimer.step = 0;
    text.finText[2].object.position.y = text.finText[2].posY[screenStat];
    gsap.to(showFinTimer, { duration: 1, step: 1, ease: "power1.out", onUpdate: function() {
      showSeparatedText(text.finText[1], showFinTimer.step);
      if (screenStat == 1) {
        showSeparatedText(text.finText[2], showFinTimer.step);
      }
    }, onComplete: function() {
      icon.scroll.material.color.setHex(0x000000);
      gsap.to(icon.scroll.scale, { duration: 0.3, x: 1, y: 1, ease: "power1.out" });
      gsap.to(icon.scroll.material, { duration: 0.3, opacity: 1, ease: "power1.out" });
      onFin = true;
      finScreenContainer.startY = 0;
      finScreenPan = true;
      finScreenReady = true;
    } });
  } });
}
let particleTween = [];
let particleTween1 = [];
function breakSymbols() {
  text.userText[1].symbol = [];
  let startPoint;
  let symbolNum = 0;
  startPoint = -text.userText[1].line[0].geometry.boundingBox.max.x;
  for (let i = inputSymbols.length; i > 0; i--) {
    text.userText[1].symbol[symbolNum] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(inputSymbols[i - 1], 0.28), 3), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 1 }));
    text.userText[1].symbol[symbolNum].geometry.computeBoundingBox();
    text.userText[1].symbol[symbolNum].geometry.translate(-text.userText[1].symbol[symbolNum].geometry.boundingBox.max.x, 0, 0);
    text.userText[1].symbol[symbolNum].position.set(startPoint + text.userText[1].line[0].geometry.boundingBox.max.x * 2 - text.userText[1].symbol[symbolNum].geometry.boundingBox.max.x / 2, text.userText[1].line[0].position.y, 0);
    text.userText[1].add(text.userText[1].symbol[symbolNum]);
    symbolNum++;
    text.userText[1].line[0].geometry.dispose();
    text.userText[1].line[0].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(inputSymbols.slice(0, inputSymbols.length - symbolNum), 0.28), 3);
    text.userText[1].line[0].geometry.computeBoundingBox();
    text.userText[1].line[0].geometry.translate(-text.userText[1].line[0].geometry.boundingBox.max.x / 2, 0, 0);
  }
  text.userText[1].line[0].geometry.NeedUpdate = true;
  text.userText[0].symbol = [];
  symbolNum = 0;
  if (text.userText[0].line[2].geometry.boundingBox.max.x > 0) {
    startPoint = -text.userText[0].line[2].geometry.boundingBox.max.x;
    for (let i = inputSymbols.length; i >= text.userText[0].wrapPoint1 + 1; i--) {
      text.userText[0].symbol[symbolNum] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(inputSymbols[i - 1], 0.28), 3), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 1 }));
      text.userText[0].symbol[symbolNum].geometry.computeBoundingBox();
      text.userText[0].symbol[symbolNum].geometry.translate(-text.userText[0].symbol[symbolNum].geometry.boundingBox.max.x, 0, 0);
      text.userText[0].symbol[symbolNum].position.set(startPoint + text.userText[0].line[2].geometry.boundingBox.max.x * 2 - text.userText[0].symbol[symbolNum].geometry.boundingBox.max.x / 2, text.userText[0].line[2].position.y, 0);
      text.userText[0].add(text.userText[0].symbol[symbolNum]);
      symbolNum ++;
      text.userText[0].line[2].geometry.dispose();
      text.userText[0].line[2].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(inputSymbols.slice(text.userText[0].wrapPoint1, inputSymbols.length - symbolNum), 0.28), 3);
      text.userText[0].line[2].geometry.computeBoundingBox();
      text.userText[0].line[2].geometry.translate(-text.userText[0].line[2].geometry.boundingBox.max.x / 2, 0, 0);
    }
    text.userText[0].line[2].geometry.NeedUpdate = true;
  } else {
    text.userText[0].wrapPoint1 = inputSymbols.length;
  }
  //alert(text.userText[0].line[1].geometry.boundingBox.max.x)
  if (text.userText[0].line[1].geometry.boundingBox.max.x > 0) {
    startPoint = -text.userText[0].line[1].geometry.boundingBox.max.x;
    for (let i = text.userText[0].wrapPoint1; i >= text.userText[0].wrapPoint + 1; i--) {
      text.userText[0].symbol[symbolNum] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(inputSymbols[i - 1], 0.28), 3), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 1 }));
      text.userText[0].symbol[symbolNum].geometry.computeBoundingBox();
      text.userText[0].symbol[symbolNum].geometry.translate(-text.userText[0].symbol[symbolNum].geometry.boundingBox.max.x, 0, 0);
      text.userText[0].symbol[symbolNum].position.set(startPoint + text.userText[0].line[1].geometry.boundingBox.max.x * 2 - text.userText[0].symbol[symbolNum].geometry.boundingBox.max.x / 2, text.userText[0].line[1].position.y, 0);
      text.userText[0].add(text.userText[0].symbol[symbolNum]);
      symbolNum++;
      //alert (symbolNum)
      text.userText[0].line[1].geometry.dispose();
      text.userText[0].line[1].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(inputSymbols.slice(text.userText[0].wrapPoint, inputSymbols.length - symbolNum), 0.28), 3);
      text.userText[0].line[1].geometry.computeBoundingBox();
      text.userText[0].line[1].geometry.translate(-text.userText[0].line[1].geometry.boundingBox.max.x / 2, 0, 0);
    }
    text.userText[0].line[1].geometry.NeedUpdate = true;
    //text.userText[0].wrapPoint1 = text.userText[0].wrapPoint;
  } else {
    
    text.userText[0].wrapPoint = inputSymbols.length;
  }
  startPoint = -text.userText[0].line[0].geometry.boundingBox.max.x;
  for (let i = text.userText[0].wrapPoint; i > 0; i--) {
  
    text.userText[0].symbol[symbolNum] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(inputSymbols[i - 1], 0.28), 3), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 1 }));
    text.userText[0].symbol[symbolNum].geometry.computeBoundingBox();
    text.userText[0].symbol[symbolNum].geometry.translate(-text.userText[0].symbol[symbolNum].geometry.boundingBox.max.x, 0, 0);
    text.userText[0].symbol[symbolNum].position.set(startPoint + text.userText[0].line[0].geometry.boundingBox.max.x * 2 - text.userText[0].symbol[symbolNum].geometry.boundingBox.max.x / 2, text.userText[0].line[0].position.y, 0);
    text.userText[0].add(text.userText[0].symbol[symbolNum]);
    symbolNum++;
  //  alert ('t' + symbolNum)
    text.userText[0].line[0].geometry.dispose();
    text.userText[0].line[0].geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(inputSymbols.slice(0, inputSymbols.length - symbolNum), 0.28), 3);
    text.userText[0].line[0].geometry.computeBoundingBox();
    text.userText[0].line[0].geometry.translate(-text.userText[0].line[0].geometry.boundingBox.max.x / 2, 0, 0);
  }
  text.userText[0].line[0].geometry.NeedUpdate = true;
  text.userText[0].newLine.count = 0;
  text.userText[0].container.width = 0;
  let wrapPoint = 0;
  startPoint = 0;
  let tessellateModifier;
  if (inputSymbols.length < 8) {
    tessellateModifier = new THREE.TessellateModifier( 0.2 / 12 * inputSymbols.length, 6 );
  } else {
    tessellateModifier = new THREE.TessellateModifier( 0.2, 6 );
  }
  while (wrapPoint < inputSymbols.length) {
    wrapPoint = 0;
    for (let i = startPoint; i < startPoint + 10; i++) {
      if (i < inputSymbols.length) {
        if (inputSymbols[i] == ` `) {
          wrapPoint = i;
        }
      } else {
        wrapPoint = inputSymbols.length;
      }
    }
    if (wrapPoint == 0) {
      wrapPoint = startPoint + 7;
    }
    text.userText[0].newLine[text.userText[0].newLine.count].geometry.dispose();
    text.userText[0].newLine[text.userText[0].newLine.count].geometry = tessellateModifier.modify(new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(inputSymbols.slice(startPoint, wrapPoint).toUpperCase(), 0.50), 3));
  //  text.userText[0].newLine[text.userText[0].newLine.count].geometry = new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(inputSymbols.slice(startPoint, wrapPoint).toUpperCase(), 0.50), 3);
   
    text.userText[0].newLine[text.userText[0].newLine.count].geometry.computeBoundingBox();
    if (text.userText[0].newLine[text.userText[0].newLine.count].geometry.boundingBox.max.x > text.userText[0].container.width) {
      text.userText[0].container.width = text.userText[0].newLine[text.userText[0].newLine.count].geometry.boundingBox.max.x;
    }
    text.userText[0].newLine[text.userText[0].newLine.count].geometry.translate(-text.userText[0].newLine[text.userText[0].newLine.count].geometry.boundingBox.max.x / 2, 0, 0);
    text.userText[0].newLine[text.userText[0].newLine.count].geometry.NeedUpdate = true;
    text.userText[0].newLine.count ++;
    if (wrapPoint < inputSymbols.length) {
      if (inputSymbols[wrapPoint] == ` `) {
        startPoint = wrapPoint + 1;
      } else {
        startPoint = wrapPoint;
      }
    }
       
      
    
  }
  
  let positionAttribute = [];
  
  for (let k = 0; k < text.userText[0].newLine.count; k++) {
    particleTween[k] = [];
    
    positionAttribute[k] = text.userText[0].newLine[k].geometry.getAttribute('position');
    const vertex = [];
//const vertex = new THREE.Vector3();
  
  for (let i = 0; i < positionAttribute[k].count; i += 3) {
    const randomX = -1.5 + Math.random() * 3;
    const randomY = -1.5 + Math.random() * 3;
    const time = {t: 0};
    
    const  timeDelay = 5 + Math.random() * 50;
   
  
    const timeRandom = 5 + Math.random() * 30;
    const num = i
//random = 0.05 * i
    let vertex = [new THREE.Vector3(), new THREE.Vector3(), new THREE.Vector3()];
    for (let j = 0; j < 3; j ++) {
      vertex[j].fromBufferAttribute(positionAttribute[k], i + j); // read vertex
    }
    //gsap.to(geometry.attributes.position.setXYZ(num + 0), { duration: 20, x: 1, y: -1, ease: "power1.inOut", delay: 2 });

    
    particleTween[k][i] = gsap.to(time, { duration: timeRandom, t: 1, ease: "power1.inOut", delay: timeDelay, onUpdate: function() {
      text.userText[0].newLine[k].geometry.attributes.position.setXYZ(num + 0, vertex[0].x + randomX * time.t, vertex[0].y + randomY * time.t, vertex[0].z); // write coordinates back
      text.userText[0].newLine[k].geometry.attributes.position.setXYZ(num + 1, vertex[1].x + randomX * time.t, vertex[1].y + randomY * time.t, vertex[1].z); // write coordinates back
      text.userText[0].newLine[k].geometry.attributes.position.setXYZ(num + 2, vertex[2].x + randomX * time.t, vertex[2].y + randomY * time.t, vertex[2].z); // write coordinates back

      text.userText[0].newLine[k].geometry.attributes.position.needsUpdate = true;

      

    
      
    } });
    
  }
  }
  text.userText[0].container.scale.set(6.5 / text.userText[0].container.width, 6.5 / text.userText[0].container.width, 1);

  
  



text.userText[1].newLine.count = 0;
  text.userText[1].container.width = 0;
  wrapPoint = 0;
  startPoint = 0;
  
  let tessellateModifier1;
  if (inputSymbols.length < 12) {
    tessellateModifier1 = new THREE.TessellateModifier( 0.2 / 12 * inputSymbols.length, 6 );

  } else {
    tessellateModifier1 = new THREE.TessellateModifier( 0.2, 6 );

  }
  
  while (wrapPoint < inputSymbols.length) {
    wrapPoint = 0;
    for (let i = startPoint; i < startPoint + 20; i++) {
      if (i < inputSymbols.length) {
        if (inputSymbols[i] == ` `) {
          wrapPoint = i;
        }
      } else {
        wrapPoint = inputSymbols.length;
      }
    }
    if (wrapPoint == 0) {
      wrapPoint = startPoint + 7;
    }
    
    text.userText[1].newLine[text.userText[1].newLine.count].geometry.dispose();
    text.userText[1].newLine[text.userText[1].newLine.count].geometry = tessellateModifier1.modify(new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(inputSymbols.slice(startPoint, wrapPoint).toUpperCase(), 0.50), 3));
  //  text.userText[0].newLine[text.userText[0].newLine.count].geometry = new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(inputSymbols.slice(startPoint, wrapPoint).toUpperCase(), 0.50), 3);
   
    text.userText[1].newLine[text.userText[1].newLine.count].geometry.computeBoundingBox();
    if (text.userText[1].newLine[text.userText[1].newLine.count].geometry.boundingBox.max.x > text.userText[1].container.width) {
      text.userText[1].container.width = text.userText[1].newLine[text.userText[1].newLine.count].geometry.boundingBox.max.x;
    }
    text.userText[1].newLine[text.userText[1].newLine.count].geometry.translate(-text.userText[1].newLine[text.userText[1].newLine.count].geometry.boundingBox.max.x / 2, 0, 0);
    text.userText[1].newLine[text.userText[1].newLine.count].geometry.NeedUpdate = true;
    text.userText[1].newLine.count ++;
    if (wrapPoint < inputSymbols.length) {
      if (inputSymbols[wrapPoint] == ` `) {
        startPoint = wrapPoint + 1;
      } else {
        startPoint = wrapPoint;
      }
    }
       
      
    
  }
  
 let positionAttribute1 = [];
  
  for (let k = 0; k < text.userText[1].newLine.count; k++) {
    particleTween1[k] = [];
    
    positionAttribute1[k] = text.userText[1].newLine[k].geometry.getAttribute('position');
    const vertex = [];
//const vertex = new THREE.Vector3();
  
  for (let i = 0; i < positionAttribute1[k].count; i += 3) {
    const randomX = -1.5 + Math.random() * 3;
    const randomY = -1.5 + Math.random() * 3;
    const time = {t: 0};
    
    const  timeDelay = 5 + Math.random() * 50;
   
  
    const timeRandom = 5 + Math.random() * 30;
    const num = i
//random = 0.05 * i
    let vertex = [new THREE.Vector3(), new THREE.Vector3(), new THREE.Vector3()];
    for (let j = 0; j < 3; j ++) {
      vertex[j].fromBufferAttribute(positionAttribute1[k], i + j); // read vertex
    }
    //gsap.to(geometry.attributes.position.setXYZ(num + 0), { duration: 20, x: 1, y: -1, ease: "power1.inOut", delay: 2 });

    
    particleTween1[k][i] = gsap.to(time, { duration: timeRandom, t: 1, ease: "power1.inOut", delay: timeDelay, onUpdate: function() {
      text.userText[1].newLine[k].geometry.attributes.position.setXYZ(num + 0, vertex[0].x + randomX * time.t, vertex[0].y + randomY * time.t, vertex[0].z); // write coordinates back
      text.userText[1].newLine[k].geometry.attributes.position.setXYZ(num + 1, vertex[1].x + randomX * time.t, vertex[1].y + randomY * time.t, vertex[1].z); // write coordinates back
      text.userText[1].newLine[k].geometry.attributes.position.setXYZ(num + 2, vertex[2].x + randomX * time.t, vertex[2].y + randomY * time.t, vertex[2].z); // write coordinates back

      text.userText[1].newLine[k].geometry.attributes.position.needsUpdate = true;

      

    
      
    } });
    
    }
  }
  text.userText[1].container.scale.set(13 / text.userText[1].container.width, 13 / text.userText[1].container.width, 1);
}
window.addEventListener('resize', onWindowResize, false);
onWindowResize();
function onWindowResize() {
  scrollProduct = false;
  if (window.innerHeight / window.innerWidth > MAIN_CAMERA_BASIC_DIMENSION) {
    screenStat = 0;
  
    mainCamera.fov = (Math.atan((window.innerHeight / 2) / ((window.innerWidth / 2) / (Math.tan(MAIN_CAMERA_ALBUM_FOV * Math.PI / 360) / MAIN_CAMERA_BASIC_DIMENSION)))) * 360 / Math.PI * mainCameraFovFactor;
    if (mainCamera.fov < 60) mainCamera.fov = 60;
   // mainCamera.fov = 57
  } else {
    screenStat = 1;
    mainCamera.fov = MAIN_CAMERA_ALBUM_FOV * mainCameraFovFactor;
  }
  
  if (screenStat == 1 && window.innerHeight / window.innerWidth > 1) {
    screeenStat = 0;
    mainCamera.fov = 60;
  }
  
  
  if (graphicsReady) {
    text.introText[0].topPart.object.position.y = text.introText[0].topPart.objectPos[screenStat];
    for (let i = 0; i < 4; i++) {
      for (let j = 0; j < text.introText[i].text.length; j++) {
        text.introText[i].text[j].position.set(text.introText[i].text[j].pos.X[screenStat], text.introText[i].text[j].pos.Y[screenStat], 0);
      }
    }
    text.inputText[0].posY[0] = Math.tan(mainCamera.fov * Math.PI / 360) * 12 - 1.4;
    text.inputText[1].posY[0] = Math.tan(mainCamera.fov * Math.PI / 360) * 12 - 3;
  
    for (let j = 0; j < 2; j++) {
      text.introText[0].topPart.text[j].position.set(text.introText[0].topPart.text[j].pos.X[screenStat], text.introText[0].topPart.text[j].pos.Y[screenStat], 0);
      text.inputText[j].object.position.y = -4 * 10 * introInterval + text.inputText[j].posY[screenStat];
    
    
      //text.inputText[j].object.position.y = -4 * 10 * introInterval + text.inputText[j].posY[0];
 
      for (let i = 0; i < text.inputText[j].text.length; i++) {
        text.inputText[j].text[i].position.set(text.inputText[j].text[i].pos.X[screenStat], text.inputText[j].text[i].pos.Y[screenStat], 0);
      }
    }
    userInput.field[screenStat].visible = true;
    userInput.field[Math.abs(screenStat - 1)].visible = false;
    text.userText[screenStat].visible = true;
    text.userText[Math.abs(screenStat - 1)].visible = false;
    text.footer[screenStat].visible = true;
    text.footer[Math.abs(screenStat - 1)].visible = false;
    if (onMovie) {
      text.userText[screenStat].container.visible = true;
      text.userText[Math.abs(screenStat - 1)].container.visible = false;
    }
    
    
    button.sound.position.set((Math.tan(mainCamera.fov * Math.PI / 360) * 12) / window.innerHeight * window.innerWidth - 0.7, -0.7, 0);
    
    text.finText[0].object.position.y = text.finText[0].posY[screenStat];

    text.finText[1].object.position.y = text.finText[1].posY[screenStat];
    text.finText[2].object.position.y = text.finText[2].posY[screenStat];

    text.finText[2].object.scale.set(text.finText[2].screenScale[screenStat], text.finText[2].screenScale[screenStat], 1);
   
   
    text.finText[3].object.position.y = text.finText[3].posY[screenStat];
    text.finText[3].object.scale.set(text.finText[3].screenScale[screenStat], text.finText[3].screenScale[screenStat], 1);

   
    
    productContainer.position.y = productContainer.posY[screenStat];
    productContainer.scale.set(productContainer.screenScale[screenStat], productContainer.screenScale[screenStat], 1);
    
    button.product_1.position.y = button.product_1.posY[screenStat];
    button.product_1.scale.set(button.product_1.screenScale[screenStat], button.product_1.screenScale[screenStat], 1);
 
 //if (onFinScreen) {
    if (screenStat == 0) {
      for (let i = 0; i < 5; i++) {
        productContainer.productPic[i].pic.material.opacity = 1;
      }
      if (Math.abs(finScreenContainer.position.y - 10 * finInterval[1]) < 10 * finInterval[1]) {
        finScreenContainer.position.y = 20 * finInterval[0];
        
       /* for (let i = 0; i < 5; i++) {
          productContainer.productPic[i].dot.material.opacity = 1;
        }*/
        
      } else {
        if (finScreenContainer.position.y > 10 * finInterval[1] && onFin) {
         // gsap.to(icon.scroll.scale, { duration: 0.3, x: 1, y: 1, ease: "power1.out" });
         // gsap.to(icon.scroll.material, { duration: 0.3, opacity: 1, ease: "none" });
     
          finScreenContainer.position.y = 30 * finInterval[0];
          for (let i = 0; i < 5; i++) {
            productContainer.productPic[i].pic.material.opacity = 0;
          }
        } else {
          finScreenContainer.position.y = 0;
          
         // gsap.to(icon.scroll.scale, { duration: 0.3, x: 1, y: 1, ease: "power1.out" });
         // gsap.to(icon.scroll.material, { duration: 0.3, opacity: 1, ease: "none" });
     
         /* for (let i = 0; i < 5; i++) {
            productContainer.productPic[i].dot.material.opacity = 1;
          }*/
        }
      }
      
      button.product_2.visible = true;
      button.product_3.visible = false;
    } else {
      
      for (let i = 0; i < 5; i++) {
        productContainer.productPic[i].pic.material.opacity = 0;
      }
      if (Math.abs(finScreenContainer.position.y - 20 * finInterval[0]) < 10 * finInterval[0]) {
        finScreenContainer.position.y = 10 * finInterval[1];
         for (let i = 0; i < 5; i++) {
           productContainer.productPic[i].dot.material.opacity = 1;
         }
      
      } else {
        if (finScreenContainer.position.y > 20 * finInterval[0]) {
         
          finScreenContainer.position.y = 20 * finInterval[1];
        if (onFin) {
          icon.scroll.scale.set(0, 0, 1);
          icon.scroll.material.opacity = 0;
        }
        /*  for (let i = 0; i < 5; i++) {
            productContainer.productPic[i].pic.material.opacity = 0;
          }*/
        } else {
          finScreenContainer.position.y = 0;
          if (onFin) {
          showSeparatedText(text.finText[2], 1);
          
          gsap.to(icon.scroll.scale, { duration: 0.3, x: 1, y: 1, ease: "power1.out" });
          gsap.to(icon.scroll.material, { duration: 0.3, opacity: 1, ease: "none" });
          }
         if (icon.scroll.scale.x < 1 && onFin) {
           gsap.to(icon.scroll.scale, { duration: 0.3, x: 1, y: 1, ease: "power1.out" });
           gsap.to(icon.scroll.material, { duration: 0.3, opacity: 1, ease: "none" });
          }
          /* for (let i = 0; i < 5; i++) {
             productContainer.productPic[i].dot.material.opacity = 1;
           }*/
        }
      }
      button.product_2.visible = false;
      button.product_3.visible = true;
    }
  }
//}
  
  frontScene.position.y = 11;
 // frontScene.position.z = 7
  waterBottomContainer.position.z = Math.tan(mainCamera.fov * Math.PI / 360) * 12;
  frontBottomContainer.position.z = Math.tan(mainCamera.fov * Math.PI / 360) * 12;

  frontTopContainer.position.z = -Math.tan(mainCamera.fov * Math.PI / 360) * 12;
  mainCamera.aspect = window.innerWidth / window.innerHeight;
  mainCamera.updateProjectionMatrix();
    
 
  mainRenderer.setSize(window.innerWidth, window.innerHeight);
  //CSSRenderer.setSize(window.innerWidth, window.innerHeight);
}









const topRendererComposer1 = new THREE.EffectComposer( mainRenderer );
topRendererComposer1.addPass( new THREE.RenderPass( mainScene, mainCamera ) );
topRendererComposer1.setSize(window.innerWidth, window.innerHeight);
const hblur = new THREE.ShaderPass(THREE.HorizontalBlurShader);
topRendererComposer1.addPass(hblur);
const vblur = new THREE.ShaderPass(THREE.VerticalBlurShader);
vblur.uniforms.v.value = 0;
hblur.uniforms.h.value = 0;
topRendererComposer1.addPass(vblur);


var stats = new Stats();
stats.showPanel(0);
//document.body.appendChild(stats.dom);
loop();
function loop() {
  if (graphicsReady) {
    let sizeY 
    if (screenStat == 0) {
      sizeY = 1 - Math.abs(14 - finScreenContainer.position.y) / 7;
    } else {
      sizeY = 1 - Math.abs(10 * finInterval[1] - finScreenContainer.position.y) / (10 * finInterval[1]);
    }
    if (sizeY < 0) sizeY = 0;
      for (let i = 0; i < 5; i++) {
        if (Math.abs(productContainer.products.position.x + productContainer.productPic[i].text[0].object.position.x) <= 5.1) {
          showSeparatedText(productContainer.productPic[i].text[0], (1 - Math.abs(productContainer.products.position.x + productContainer.productPic[i].text[0].object.position.x) / 5.1) * sizeY);
          showSeparatedText(productContainer.productPic[i].text[1], (1 - Math.abs(productContainer.products.position.x + productContainer.productPic[i].text[0].object.position.x) / 5.1) * sizeY);
        } else {
          showSeparatedText(productContainer.productPic[i].text[0], 0);
          showSeparatedText(productContainer.productPic[i].text[1], 0);
        }
      }
      if (onFin) {
        svgPic[0].position.y = svgPic[0].posY + finScreenContainer.position.y;
        for (let i = 0; i < 5; i++) {
          if (Math.abs(productContainer.products.position.x + productContainer.productPic[i].position.x) < 5.1) {
            productContainer.productPic[i].pic.scale.set(1 + (1 - Math.abs(productContainer.products.position.x + productContainer.productPic[i].position.x) / 5.1) * 0.035, 1 + (1 - Math.abs(productContainer.products.position.x + productContainer.productPic[i].position.x) / 5.1) * 0.035, 1);
            productContainer.productPic[i].dot.scale.set(1 + (1 - Math.abs(productContainer.products.position.x + productContainer.productPic[i].position.x) / 5.1) * 1.5, 1 + (1 - Math.abs(productContainer.products.position.x + productContainer.productPic[i].position.x) / 5.1) * 1.5, 1);
          } else {
        }
      
      if (screenStat == 0) {
        if (Math.abs(10 * finInterval[0] - finScreenContainer.position.y) / (10 * finInterval[0]) >= 0) {
          showSeparatedText(text.finText[2], (1 - Math.abs(10 * finInterval[0] - finScreenContainer.position.y) / (10 * finInterval[0])));
          showSeparatedText(text.finText[1], (Math.abs(10 * finInterval[0] - finScreenContainer.position.y) / (10 * finInterval[0])));
          showSeparatedText(text.finText[0], (Math.abs(10 * finInterval[0] - finScreenContainer.position.y) / (10 * finInterval[0])));
        }
        
       /* if (Math.abs(20 * finInterval[0] - finScreenContainer.position.y) >= 10 * finInterval[0]) {
    
          document.getElementById('gradient').style.background = `linear-gradient(0deg, rgba(255, 255, 255, 0.95) 65%, rgba(${48 + 207 * (Math.abs(finScreenContainer.position.y - 20 * finInterval[0]) / (10 * finInterval[0]))}, ${174 + 81 * (Math.abs(finScreenContainer.position.y - 20 * finInterval[0]) / (10 * finInterval[0]))}, ${191 + 64 * (Math.abs(finScreenContainer.position.y - 20 * finInterval[0]) / (10 * finInterval[0]))}, ${1 - 0.2 * (Math.abs(finScreenContainer.position.y - 20 * finInterval[0]) / (10 * finInterval[0]))}))`;
        }*/
        
        text.footer[0].position.y = -25.2 * finInterval[0] + finScreenContainer.position.y;
      
         
        if (Math.abs(finScreenContainer.position.y - 20 * finInterval[0]) <= 10 * finInterval[0]) { 
          button.product_1.text.scale.set(1 - Math.abs(finScreenContainer.position.y - 20 * finInterval[0]) / (10 * finInterval[0]), 1 - Math.abs(finScreenContainer.position.y - 20 * finInterval[0]) / (10 * finInterval[0]), 1);
          button.product_1.text.material.opacity = 1 - Math.abs(finScreenContainer.position.y - 20 * finInterval[0]) / (10 * finInterval[0]);
          if (Math.abs(finScreenContainer.position.y <= 20 * finInterval[0])) {
     
            document.getElementById('gradient').style.background = `linear-gradient(0deg, rgba(255, 255, 255, 0.95) 65%, rgba(${48 + 207 * (1 - Math.abs(finScreenContainer.position.y - 20 * finInterval[0]) / (10 * finInterval[0]))}, ${174 + 81 * (1 - Math.abs(finScreenContainer.position.y - 20 * finInterval[0]) / (10 * finInterval[0]))}, ${191 + 64 * (1 - Math.abs(finScreenContainer.position.y - 20 * finInterval[0]) / (10 * finInterval[0]))}, ${0.8 + 0.15 * (1 - Math.abs(finScreenContainer.position.y - 20 * finInterval[0]) / (10 * finInterval[0]))}))`;
          }
          if (Math.abs(finScreenContainer.position.y > 20 * finInterval[0])) {
            for (let i = 0; i < 5; i++) {
              productContainer.productPic[i].dot.material.opacity = 1 - Math.abs(finScreenContainer.position.y - 20 * finInterval[0]) / (10 * finInterval[0]);
              productContainer.productPic[i].pic.material.opacity = 1 - Math.abs(finScreenContainer.position.y - 20 * finInterval[0]) / (10 * finInterval[0]);
            }
          }
          if (Math.abs(finScreenContainer.position.y > 10 * finInterval[0])) {
            for (let i = 0; i < 5; i++) {
              productContainer.productPic[i].dot.material.opacity = 1 - Math.abs(finScreenContainer.position.y - 20 * finInterval[0]) / (10 * finInterval[0]);
            }
          }
        }
        if (Math.abs(finScreenContainer.position.y - 30 * finInterval[0]) <= 10 * finInterval[0]) {
          button.product_2.text.material.opacity = 1 - Math.abs(finScreenContainer.position.y - 30 * finInterval[0]) / (10 * finInterval[0]);
          button.product_2.border.material.opacity = 1 - Math.abs(finScreenContainer.position.y - 30 * finInterval[0]) / (10 * finInterval[0]);
          button.product_2.scale.set(1 - Math.abs(finScreenContainer.position.y - 30 * finInterval[0]) / (10 * finInterval[0]), 1 - Math.abs(finScreenContainer.position.y - 30 * finInterval[0]) / (10 * finInterval[0]), 1);
          showSeparatedText(text.finText[3], (1 - Math.abs(finScreenContainer.position.y - 30 * finInterval[0]) / (10 * finInterval[0])));
       
          if (Math.abs(finScreenContainer.position.y <= 30 * finInterval[0])) {
            document.getElementById('gradient').style.background = `linear-gradient(180deg, rgba(255, 255, 255, 0.95) 30%,  rgba(${255 - 108 * (1 - Math.abs(finScreenContainer.position.y - 30 * finInterval[0]) / (10 * finInterval[0]))}, ${255 - 44 * (1 - Math.abs(finScreenContainer.position.y - 30 * finInterval[0]) / (10 * finInterval[0]))}, ${255 - 205 * (1 - Math.abs(finScreenContainer.position.y - 30 * finInterval[0]) / (10 * finInterval[0]))}, ${0.95 - 0.15 * (1 - Math.abs(finScreenContainer.position.y - 30 * finInterval[0]) / (10 * finInterval[0]))}))`;

          }
           
        }
      } else {
        
        text.footer[1].position.y = -18 * finInterval[1] + finScreenContainer.position.y;
    
     
        if (Math.abs(10 * finInterval[1] - finScreenContainer.position.y) / (10 * finInterval[0]) >= 0) {
         // showSeparatedText(text.finText[2], (Math.abs(10 * finInterval[1] - finScreenContainer.position.y) / (10 * finInterval[1])));
          showSeparatedText(text.finText[1], (Math.abs(10 * finInterval[1] - finScreenContainer.position.y) / (10 * finInterval[1])));
          showSeparatedText(text.finText[0], (Math.abs(10 * finInterval[1] - finScreenContainer.position.y) / (10 * finInterval[1])));
          if (Math.abs(finScreenContainer.position.y <= 10 * finInterval[1])) {
          
            document.getElementById('gradient').style.background = `linear-gradient(0deg, rgba(255, 255, 255, 0.95) 65%, rgba(${48 + 207 * (1 - Math.abs(finScreenContainer.position.y - 10 * finInterval[1]) / (10 * finInterval[1]))}, ${174 + 81 * (1 - Math.abs(finScreenContainer.position.y - 10 * finInterval[1]) / (10 * finInterval[1]))}, ${191 + 64 * (1 - Math.abs(finScreenContainer.position.y - 10 * finInterval[1]) / (10 * finInterval[1]))}, ${0.8 + 0.15 * (1 - Math.abs(finScreenContainer.position.y - 10 * finInterval[1]) / (10 * finInterval[1]))}))`;
          }
          for (let i = 0; i < 5; i++) {
            productContainer.productPic[i].dot.material.opacity = 1 - Math.abs(finScreenContainer.position.y - 10 * finInterval[1]) / (10 * finInterval[1]);
            productContainer.productPic[i].pic.material.opacity = 1 - Math.abs(finScreenContainer.position.y - 10 * finInterval[1]) / (10 * finInterval[1]);
          }
      
      
        }
        if (Math.abs(20 * finInterval[1] - finScreenContainer.position.y) / (10 * finInterval[1]) <=10 * finInterval[1]) {
          showSeparatedText(text.finText[3], (1 - Math.abs(20 * finInterval[1] - finScreenContainer.position.y) / (10 * finInterval[1])));
          if (finScreenContainer.position.y >= 10 * finInterval[1]) {
        
             showSeparatedText(text.finText[2], (Math.abs(20 * finInterval[1] - finScreenContainer.position.y) / (10 * finInterval[1])));
          }
          
          button.product_3.text.material.opacity = 1 - Math.abs(finScreenContainer.position.y - 20 * finInterval[1]) / (10 * finInterval[1]);
          button.product_3.border.material.opacity = 1 - Math.abs(finScreenContainer.position.y - 20 * finInterval[1]) / (10 * finInterval[1]);
          button.product_3.scale.set((1 - Math.abs(finScreenContainer.position.y - 20 * finInterval[1]) / (10 * finInterval[0])) * 0.8, (1 - Math.abs(finScreenContainer.position.y - 20 * finInterval[1]) / (10 * finInterval[1])) * 0.8, 1);
          
          if (Math.abs(finScreenContainer.position.y > 10 * finInterval[1])) {
            
            document.getElementById('gradient').style.background = `linear-gradient(180deg, rgba(255, 255, 255, 0.95) 30%,  rgba(${255 - 108 * (1 - Math.abs(finScreenContainer.position.y - 20 * finInterval[1]) / (10 * finInterval[1]))}, ${255 - 44 * (1 - Math.abs(finScreenContainer.position.y - 20 * finInterval[1]) / (10 * finInterval[1]))}, ${255 - 205 * (1 - Math.abs(finScreenContainer.position.y - 20 * finInterval[1]) / (10 * finInterval[1]))}, ${0.95 - 0.15 * (1 - Math.abs(finScreenContainer.position.y - 20 * finInterval[1]) / (10 * finInterval[1]))}))`;
          
          }
        }


      //  text.footer[0].position.y = -35.2 * finInterval[0] + finScreenContainer.position.y;
        if (Math.abs(finScreenContainer.position.y) > 10 * finInterval[1]) {
          button.product_1.text.scale.set(1- Math.abs(finScreenContainer.position.y - 10 * finInterval[1]) / (10 * finInterval[1]), 1 - Math.abs(finScreenContainer.position.y - 10 * finInterval[1]) / (10 * finInterval[1]), 1);
          button.product_1.text.material.opacity = 1 - Math.abs(finScreenContainer.position.y - 10 * finInterval[1]) / (10 * finInterval[1]);
          if (Math.abs(finScreenContainer.position.y > 10 * finInterval[1])) {
           /* for (let i = 0; i < 5; i++) {
              productContainer.productPic[i].dot.material.opacity = 1 - Math.abs(finScreenContainer.position.y - 10 * finInterval[1]) / (10 * finInterval[1]);
              productContainer.productPic[i].pic.material.opacity = 1 - Math.abs(finScreenContainer.position.y - 10 * finInterval[1]) / (10 * finInterval[1]);
            }*/
          }
          if (Math.abs(finScreenContainer.position.y > 0)) {
            for (let i = 0; i < 5; i++) {
              productContainer.productPic[i].dot.material.opacity = 1 - Math.abs(finScreenContainer.position.y - 10 * finInterval[1]) / (10 * finInterval[1]);
            }
          }
        }
        /*if (Math.abs(finScreenContainer.position.y - 30 * finInterval[0]) <= 10 * finInterval[0]) {
          button.product_2.text.material.opacity = 1 - Math.abs(finScreenContainer.position.y - 30 * finInterval[0]) / (10 * finInterval[0]);
          button.product_2.border.material.opacity = 1 - Math.abs(finScreenContainer.position.y - 30 * finInterval[0]) / (10 * finInterval[0]);
          button.product_2.scale.set((1 - Math.abs(finScreenContainer.position.y - 30 * finInterval[0]) / (10 * finInterval[0])) * 0.1, (1 - Math.abs(finScreenContainer.position.y - 30 * finInterval[0]) / (10 * finInterval[0])) * 0.1, 1);
        }*/
      }
        }
    }
    
    if (onIntro) {
      frontStartScreenContainer.position.y = startScreenContainer.position.y;
      for (let i = 0; i < 4; i++) {
        if (Math.abs(startScreenContainer.position.y + text.introText[i].object.position.y) <= 10 * introInterval) {
          showSeparatedText(text.introText[i], (1 - Math.abs(startScreenContainer.position.y + text.introText[i].object.position.y) / (10 * introInterval)));
          if (i > 0) {
            for (let j = 0; j < text.introText[i].topPart.part.length; j++) {
              text.introText[i].topPart.part[j].material.opacity = (1 - Math.abs(startScreenContainer.position.y + text.introText[i].object.position.y) / (10 * introInterval));
            }
            text.introText[i].topPart.object.scale.set((1 - Math.abs(startScreenContainer.position.y + text.introText[i].object.position.y) / 10 * introInterval) * 0.9, (1 - Math.abs(startScreenContainer.position.y + text.introText[i].object.position.y) / 10 * introInterval) * 0.9, 1);
          } else {
            showSeparatedText(text.introText[i].topPart, (1 - Math.abs(startScreenContainer.position.y + text.introText[i].object.position.y) / (10 * introInterval)));
          }
        } else {
          showSeparatedText(text.introText[i], 0);
          if (i > 0) {
            text.introText[i].topPart.object.scale.set(0, 0, 1);
            for (let j = 0; j < text.introText[i].topPart.part.length; j++) {
              text.introText[i].topPart.part[j].material.opacity = 0;
            }
          } else {
            showSeparatedText(text.introText[i].topPart, 0);
          }
        }
      }
      let screenOrder = Math.floor(startScreenContainer.position.y / (10 * introInterval));
      if (screenOrder < 0) screenOrder = 1;
      const screenPos = startScreenContainer.position.y % (10 * introInterval);
      if (startScreenContainer.position.y >= 0) {
        document.getElementById('gradient').style.background = `linear-gradient(rgba(255, 255, 255, 0.95) 35%, rgba(${gradientColor[screenOrder].r + gradientColor[screenOrder].rNext * screenPos / introInterval}, ${gradientColor[screenOrder].g + gradientColor[screenOrder].gNext * screenPos / introInterval}, ${gradientColor[screenOrder].b + gradientColor[screenOrder].bNext * screenPos / introInterval}, 0.8))`;
      }
      if (startScreenContainer.position.y >= 30 * introInterval) {
        if (startScreenContainer.position.y > 31 * introInterval) {
          showSeparatedText(text.inputText[0], (Math.abs(startScreenContainer.position.y - 30 * introInterval) / (10 * introInterval)));
          showSeparatedText(text.inputText[1], (Math.abs(startScreenContainer.position.y - 30 * introInterval) / (10 * introInterval)));
          goBlur = true;
          vblur.uniforms.v.value = 1 * Math.abs(1 - (-41 * introInterval + startScreenContainer.position.y) / (-10 * introInterval)) / window.innerHeight * window.devicePixelRatio;
          hblur.uniforms.h.value = 1 * Math.abs(1 - (-41 * introInterval + startScreenContainer.position.y) / (-10 * introInterval)) / window.innerWidth * window.devicePixelRatio;
          userInput.field[0].material.opacity = Math.abs(1 - (-41 * introInterval + startScreenContainer.position.y) / (-10 * introInterval));
        }
        userInput.field[0].material.opacity = Math.abs(1 - (-40 * introInterval + startScreenContainer.position.y) / (-10 * introInterval));
        userInput.field[1].material.opacity = Math.abs(1 - (-40 * introInterval + startScreenContainer.position.y) / (-10 * introInterval));
        userInput.position.y = userInput.posY - (40 * introInterval - startScreenContainer.position.y) / 2;
      }
      if (startScreenContainer.position.y < 31 * introInterval && goBlur) {
        goBlur = false;
      }
      let waterPos = Math.abs(Math.round(startScreenContainer.position.y / (10 * introInterval)) * (10 * introInterval) - startScreenContainer.position.y) * 4;
      if (startScreenContainer.position.y <= 30 * introInterval) {
        water.position.y = -1 - waterPos;
        if (water.position.y < -11) water.position.y = -11;
      } else {
        if (water.position.y < 0) water.position.y = 1;
      }
      if (startScreenContainer.position.y >= 0 && startScreenContainer.position.y <= 10 * introInterval) {
        svgPic[0].scale.set(0.02 - 0.005 * (1 - (8 - startScreenContainer.position.y) / (10 * introInterval)), -(0.02 - 0.005 * (1 - (8 - startScreenContainer.position.y) / (10 * introInterval))), 1);
        svgPic[0].position.set(-1.45 + 0.356 * (1 - (8 - startScreenContainer.position.y) / (10 * introInterval)), -0.5 + 0.2 * (1 - (8 - startScreenContainer.position.y) / (10 * introInterval)), 0);
      }
      if (startScreenContainer.position.y % (10 * introInterval) == 0) {
        water.position.y = 1;
      }
      userInput.field[0].position.y = userInput.position.y;
      userInput.field[1].position.y = userInput.position.y;
    }
  }
  //CSSField.object.position.y = userInput.position.y + userInput.fieldPosY;
  
//  CSSRenderer.render(mainScene, mainCamera);
  stats.update();
  mainRenderer.autoClear = true;
 //
 
 //topRendererComposer1.autoClear = true;
  
  

  
  if (goBlur) {
    mainCamera.near = 2;
    mainCamera.far = 21;
    mainCamera.updateProjectionMatrix();
    topRendererComposer1.render();
  } else {
    mainCamera.near = 1;
    mainCamera.far = 21;
    mainCamera.updateProjectionMatrix();
    mainRenderer.render(mainScene, mainCamera);
  }
  mainRenderer.autoClear = false;
  mainCamera.near = 1;
  mainCamera.far = 3;
  mainCamera.updateProjectionMatrix();
  mainRenderer.render(frontScene, mainCamera);
  
  //animateGradient();
  
  
  requestAnimationFrame(loop);
}
const waterText = [
 `Море<спокойствия|`,
 `Смываем усталость и накопленные|эмоции|`,
 `Случаются дни, когда всё идёт|не по плану, а мелкие<неприятности>выбивают<из колеи|`,
 `Разбитая с утра чашка кофе,|ушибленный об угол мизинец<или>пробка по пути домой|`,
 `Каждому хоть раз в жизни<хотелось,>чтобы такие досадные<мелочи просто>остались в этом<дне и больше не>повторялись|`,
 `Чтобы отпустить накопленные эмоции,<впишите в окошко то,>что вас сегодня<расстроило.|`,
 `А затем просто понаблюдайте,<как медленно и красиво|всё смывает водой|`,
 `Дождь, которого не было`,
 `в прогнозе`,
 `Дождь, которого не было в прогнозе`,
 `Вода смывает|переживания не<только>на экране|`,
 `Чтобы расслабиться и прийти в себя,<можно принять>контрастный душ, тёплую<ванну или даже просто умыться|классным средством. Такой ритуал<бережно очистит>не только кожу,<но и мысли|`,
 `Для этого мы разработали<линейку очищающих|и успокаивающих средств<под разные типы кожи|`,
 `Гель для умывания|с экстрактами шпината|и брокколи|`,
 `Пена для умывания|с водорослями и водной|лилией|`,
 `Гель для умывания кожи,|склонной к акне|`,
 `Масло для умывания|`,
 `Мицеллярная вода|для очищения|`,
 `Лёгкий травяной запах приятно|освежает с утра и напоминает|о тёплом лете, а зелёный чай|уменьшает воспаления|`,
 `Пена с водорослями — самое то|для тех, у кого есть буквально|пара минут на очищение и|увлажнение кожи. Чтобы скорее| умыться и нырнуть в дела|`,
 `Этот гель с подорожником хочется|приложить ко всем|изматывающим дням. Всё, чтобы|оставаться на спокойной волне|без воспалений на коже|`,
 `Масла манго, абрикосовых|косточек и облепихи бережно|очищают и увлажняют, а ещё|добавляют яркости коже и|настроению|`,
 `Мицеллярка здорово справляется|со стрелками, тушью и остальным|макияжем. Алоэ вера в составе|увлажняет, а пантенол — снимает|покраснения|`,
 `А чтобы смывать<накопленные эмоции<было ещё>приятнее,<дарим скидку 10%<на любое средство|при заказе от 700₽<по промокоду CALM|`,
 `Сайт со звуком|`,
 `Наденьте наушники или переключите|тумблер бесшумного режима|`,
 `Реклама, предложение действует с 15.06.2022 по 31.07.2022,`,
 `информацию о порядке пооведения акции, сроке, месте,`,
 `порядке получения и количестве товара участвующего в акции`,
 `уточняйте в условиях акции. Организатор, продавец ООО`,
 `«Умный ритейл», ОГРН 1177847261602, 192019, Санкт–Петербург,`,
 `ул.Седова, дом.11, Литер А, Этаж 6, помещение 627, Зоны`,
 `доставки и точное время доставки уточняйте в мобильном`,
 `приложении «Самокат»`,
 ` `,
 `ООО «Умный ритейл» © 2022`,
 `Реклама, предложение действует с 15.06.2022 по 31.07.2022, информацию о порядке пооведения акции, сроке, месте, порядке получения и количестве товара участвующего в акции уточняйте в условиях акции.`,
 `Организатор, продавец ООО «Умный ритейл», ОГРН 1177847261602, 192019, Санкт–Петербург, ул.Седова, дом.11, Литер А, Этаж 6, помещение 627, Зоны доставки и точное время доставки уточняйте в мобильном`,
 `приложении «Самокат»`
];


let soundStarted = false;
let touchReady = true;
const raycaster = new THREE.Raycaster(), mouse = new THREE.Vector2();
function onDocumentTouchStart(event) {
 // if (!sound.on('play', alert('ok'))) alert('no')
  //sound.play();
  event.preventDefault();
  if (!soundStarted) {
    //sound.pause();
    goSound();
  //  showSoundButton();
    document.getElementById("myVideo").play();
    soundStarted = true;
  
  }
  
  if (touchReady) {
   // alert('g')
    touchReady = false;
    if ((!startScreenPan || !finScreenPan) && stopDrag) {
      stopDrag = false;
      
    }
    
 /*   mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;*/

    
    mouse.x = (event.touches[0].clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.touches[0].clientY / window.innerHeight) * 2 + 1;
    
    raycaster.setFromCamera(mouse, mainCamera);
    checkInteractive(raycaster);
  }
}

function onDocumentTouchEnd(event) {
 // raycaster.setFromCamera(mouse, camera);
  event.preventDefault();
    raycaster.setFromCamera(mouse, mainCamera);
    intersects = raycaster.intersectObject(svgPic[0].plane);
    if (intersects.length > 0) {
    
      window.open("https://samokat.ru/");
    }
    intersects = raycaster.intersectObject(svgPic[3].plane);
    if (intersects.length > 0) {
    
      window.open("https://samokat.ru/");
    }
  intersects = raycaster.intersectObject(button.product_1.plane);
  if (intersects.length > 0 && button.product_1.text.scale.x == 1) {
    gsap.to(button.product_1.text.scale, { duration: 0.15, x: 0.8, y: 0.8, ease: "power1.out", repeat: 1, yoyo: true });
    window.open("https://vml8.adj.st/category/53e3a4b7-3c4f-467a-93e9-887fef56368d?showcaseType=MINIMARKET&adj_t=avwow4v");
    
  }

  intersects = raycaster.intersectObject(button.footer.plane);
  if (intersects.length > 0) {
  
    window.open("https://app.adjust.com/avwow4v");
    
  }
  intersects = raycaster.intersectObject(button.footer_1.plane);
  if (intersects.length > 0) {
  
    window.open("https://app.adjust.com/avwow4v");
    
  }
  intersects = raycaster.intersectObject(text.footer[0].plane);
  if (intersects.length > 0) {
  
    window.open("https://terms.samokat.ru/promo/Beauty_D2%D0%A1_junejuly.pdf");
  
  }
    
  intersects = raycaster.intersectObject(text.footer[1].plane);
  if (intersects.length > 0) {
  
    window.open("https://terms.samokat.ru/promo/Beauty_D2%D0%A1_junejuly.pdf");
  
  }
 
  // lookReady = true;
}

function onDocumentMouseDown(event) {
  
  
  event.preventDefault();
  if (!soundStarted) {
    goSound();
   // showSoundButton();
    document.getElementById("myVideo").play();
    soundStarted = true;
    
  }
  
  if (touchReady) {
    touchReady = false;
    if ((!startScreenPan || !finScreenPan) && stopDrag) {
      stopDrag = false;
  
    }
    
  
    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
    raycaster.setFromCamera(mouse, mainCamera);
    checkInteractive(raycaster);
  }
}

function checkInteractive(raycaster) {
  
  
  
  
  intersects = raycaster.intersectObject(icon.scroll.part[3]);
  if (intersects.length > 0 && icon.scroll.scale.x == 1 && (onFin || onIntro)) {
    gsap.to(icon.scroll.scale, { duration: 0.5, x: 0, y: 0, ease: "power1.out" });
    gsap.to(icon.scroll.material, { duration: 0.5, opacity: 0, ease: "none" });
    if (onIntro) {
  
    startScreenReady = false;
    gsap.to(startScreenContainer.position, { duration: 2, y: startScreenContainer.position.y + 10 * introInterval, ease: "power2.inOut", onComplete: function() {
      if (startScreenContainer.position.y <= 30 * introInterval) {
        gsap.to(icon.scroll.scale, { duration: 0.3, x: 1, y: 1, ease: "power1.out" });
        gsap.to(icon.scroll.material, { duration: 0.3, opacity: 1, ease: "none", onComplete: function() {
          startScreenReady = true;
        } });
      } else {
        startScreenReady = true;
      }
      if (startScreenContainer.position.y == 40 * introInterval) {
        if (!checkIntroEndOnce) {
          checkIntroEndOnce = true;
        }
        if (button.goMovie.opacityTween != null && button.goMovie.opacityTween != undefined) {
          button.goMovie.opacityTween.kill();
          button.goMovie.opacityTween = null;
        }
        if (symbolCount > 0) {
          button.goMovieOpasity = 1;
        } else {
          button.goMovieOpasity = 0.5;
        }
        button.goMovie.opacityTween = gsap.to([button.goMovie.text.material, button.goMovie.border.material, button.goMovie.plane.material], { duration: 0.3, opacity: button.goMovieOpasity, ease: "none" });
        userInput.ready = true;
      }
      water.position.y = 1;
    } });
    }
    if (onFin) {
      finScreenReady = false;
      gsap.to(finScreenContainer.position, { duration: 2, y: finScreenContainer.position.y + 10 * finInterval[screenStat], ease: "power2.inOut", onComplete: function() {
        finScreenReady = true;
        if (screenStat == 0) {
          if (finScreenContainer.position.y == 30 * finInterval[0]) {
            finScrollCount++;
            
          }
          if (finScreenContainer.position.y == 40 * finInterval[0] && finScrollCount == 2) {
            finScrollCount--;
          }
        } else {
          if (finScreenContainer.position.y == 20 * finInterval[1]) {
            finScrollCount++;
            
          }
        }
      
      
      } });
    
    }
  }
  
  if (!isMobile()) {
    intersects = raycaster.intersectObject(svgPic[0].plane);
    if (intersects.length > 0) {
    
      window.open("https://samokat.ru/");
    }
    intersects = raycaster.intersectObject(svgPic[3].plane);
    if (intersects.length > 0) {
    
      window.open("https://samokat.ru/");
    }
    intersects = raycaster.intersectObject(button.product_1.plane);
    if (intersects.length > 0) {
      gsap.to(button.product_1.text.scale, { duration: 0.15, x: 0.8, y: 0.8, ease: "power1.out", repeat: 1, yoyo: true });
      window.open("https://vml8.adj.st/category/53e3a4b7-3c4f-467a-93e9-887fef56368d?showcaseType=MINIMARKET&adj_t=avwow4v");
    }

    intersects = raycaster.intersectObject(button.footer.plane);
    if (intersects.length > 0) {
    
      window.open("https://app.adjust.com/avwow4v");
      
    
    }
    intersects = raycaster.intersectObject(button.footer_1.plane);
    if (intersects.length > 0) {
    
      window.open("https://app.adjust.com/avwow4v");
      
    } 
    intersects = raycaster.intersectObject(text.footer[1].plane);
    if (intersects.length > 0) {
    
      window.open("https://terms.samokat.ru/promo/Beauty_D2%D0%A1_junejuly.pdf");
    
    }
  }
  
 /* intersects = raycaster.intersectObject(svgPic[0].plane);
  if (intersects.length > 0) {
  
    window.open("https://samokat.ru/");
  
  }*/
  
  intersects = raycaster.intersectObject(button.popUp.plane);
  if (intersects.length > 0 && button.popUp.ready) {
    hidePopUp();
    document.getElementById("myVideo").play();
    button.popUp.ready = false;
    goSound();
    soundStarted = true;
    
    
    
  }

  
  
  intersects = raycaster.intersectObject(button.sound.plane);
  if (intersects.length > 0 && pic[6].opacity > 0) {
  
  if(pic[6].opacity == 1) {
    gsap.to(pic[6], { duration: 0.2, opacity: 0.3, ease: "power2.inOut" });

    sound.pause();
  } else if (pic[6].opacity == 0.3) {
    gsap.to(pic[6], { duration: 0.2, opacity: 1, ease: "power2.inOut" });
  
    sound.play();
  }
  
  }
  
  for (let i = 0; i < 5; i++) {
    intersects = raycaster.intersectObject(productContainer.productPic[i].pic);
    if (intersects.length > 0) {
     
      if (finScreenReady && finScreenContainer.position.y == 10 * finInterval[0]) {
        finScreenReady = false;
        gsap.to(finScreenContainer.position, { duration: 1, y: finScreenContainer.position.y + 10 * finInterval[screenStat], ease: "power2.inOut", onComplete: function() {
          finScreenReady = true;
      
      
      
        } });
    
      }
     if (!isMobile()){
      if (productScrollReady && productContainer.products.position.x != productContainer.productPic[i].position.x) {
     
      productScrollReady = false;
      if (productContainer.touchTween !== null && productContainer.touchTween !== undefined) {
        productContainer.touchTween.kill();
        productContainer.touchTween = null;
      }
      
      gsap.to(productContainer.products.position, { duration: 1, x: -productContainer.productPic[i].position.x, ease: "power2.inOut", onComplete: function() {
        productScrollReady = productScrollReady = true;
        if (productContainer.touchTween !== null && productContainer.touchTween !== undefined) {
          productContainer.touchTween.kill();
          productContainer.touchTween = null;
        }
        
        
      } });
      }
     }
      
    }
  }
  
  
  intersects = raycaster.intersectObject(userInput.field[0]);
  if (intersects.length > 0 && userInput.ready && userInput.field[0].visible) {
    onIntro = false;
   
      goInput();
    
    
    
  }
  intersects = raycaster.intersectObject(userInput.field[1]);
  if (intersects.length > 0 && userInput.ready && userInput.field[1].visible) {
    onIntro = false;
    goInput();
  
  }
  
  intersects = raycaster.intersectObject(productContainer.touch);
  if (intersects.length > 0 && finScreenContainer.position.y <= 14) {
    productScrollReady = true;
    
  }
  
/*  intersects = raycaster.intersectObject(text.finText[3].plane);
  if (intersects.length > 0) {
    
    copy("CALM");
    if (text.finText[3].alert.tween !== null && text.finText[3].alert.tween !== undefined) {
      text.finText[3].alert.tween.kill();
      text.finText[3].alert.tween = null;
    }
    text.finText[3].alert.material.opacity = 0;
    text.finText[3].alert.tween = gsap.to(text.finText[3].alert.material, { duration: 0.2, opacity: 1, ease: "none", onComplete: function() {
      if (text.finText[3].alert.tween !== null && text.finText[3].alert.tween !== undefined) {
        text.finText[3].alert.tween.kill();
        text.finText[3].alert.tween = null;
      }
      text.finText[3].alert.tween = gsap.to(text.finText[3].alert.material, { duration: 5, opacity: 0, ease: "power4.in" });
  
    } });

    
  
  }*/
  
  if (keyboardReady) {
    intersects = raycaster.intersectObject(keyboard.touch);
    if (intersects.length > 0 && keyboardReady) {
    
      keyboardReady = false;
     
      hideKeyboard();
    }
    
  }
  
  
  
  if (keyboard.ready) {
    
    
    
    
    intersects = raycaster.intersectObject(keyboard.touch);
    if (intersects.length > 0) {
      
      keyboard.ready = false;
     
      hideKeyboard();
    }
    intersects = raycaster.intersectObject(keyboard.touch_1);
    if (intersects.length > 0) {
      keyboard.ready = false;
    
      hideKeyboard();
    }
    intersects = raycaster.intersectObject(keyboard.touch_2);
    if (intersects.length > 0) {
      keyboard.ready = false;
    
      hideKeyboard();
    }
    intersects = raycaster.intersectObject(keyboard.button[77]);
    if (intersects.length > 0) {
      
      keyboard.ready = false;
      gsap.to(keyboard.button[77].key.material, { duration: 0.05, opacity: 0, ease: "power2.out", repeat: 1, yoyo: true, onComplete: function() {
        hideKeyboard();
      } });
    }
    for (let i = 0; i < 76; i++) {
      intersects = raycaster.intersectObject(keyboard.button[i].key);
      if (intersects.length > 0) {
        if (i == 42) {
          gsap.to(keyboard.button[i].key.material, { duration: 0.05, opacity: 0, ease: "power2.out", repeat: 1, yoyo: true });
        } else {
          gsap.to(keyboard.button[i].key.material, { duration: 0.05, opacity: 0.2, ease: "power2.out", repeat: 1, yoyo: true });
        }
        goType(i);
      }
    }
    intersects = raycaster.intersectObject(keyboard.button[76].key);
    if (intersects.length > 0) {
      gsap.to(keyboard.button[76].key.material, { duration: 0.05, opacity: 0, ease: "power2.out", repeat: 1, yoyo: true });
      keyboard.button[76].symbol.geometry.dispose();
      if (keyboard.RU.position.y > -400) {
        keyboard.RU.position.y = -400;
        keyboard.EN.position.y = 5;
        keyboard.button[76].symbol.geometry = new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(`EN`, 0.55), 3);
      } else {
        keyboard.RU.position.y = 5;
        keyboard.EN.position.y = -400;
        keyboard.button[76].symbol.geometry = new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(`RU`, 0.55), 3);
      }
      keyboard.button[76].symbol.geometry.computeBoundingBox();
      keyboard.button[76].symbol.geometry.translate(-keyboard.button[76].symbol.geometry.boundingBox.max.x / 2, -0.275, 0);
      keyboard.button[76].symbol.geometry.NeedUpdate = true;
    }
    intersects = raycaster.intersectObject(keyboard.button[78].key);
    if (intersects.length > 0) {
      if (!keyboard.upperCase) {
        keyboard.upperCase = true;
        if (keyboard.keyTween2 !== null && keyboard.keyTween2 !== undefined) {
          keyboard.keyTween2.kill();
          keyboard.keyTween2 = null;
        }
        keyboard.keyTween2 = gsap.to(keyboard.button[78].symbol2.material, { duration: 0.2, opacity: 0.02, ease: "none", repeat: 1, yoyo: true });
      } else {
        keyboard.upperCase = false;
      }
    
    
    if (keyboard.button[78].symbol2.material.opacity > 0) {
      if (keyboard.keyTween2 !== null && keyboard.keyTween2 !== undefined) {
        keyboard.keyTween2.kill();
        keyboard.keyTween2 = null;
      }
      //keyboard.button[78].symbol2.material.opacity = 0;
      gsap.to(keyboard.button[78].symbol2.material, { duration: 0.1, opacity: 1, ease: "none" });
      keyboard.UpperCaseStatic = true;
      keyboard.upperCase = true;
    }
    
    if (keyboard.button[78].symbol2.material.opacity == 1) {
      gsap.to(keyboard.button[78].symbol2.material, { duration: 0.1, opacity: 0, ease: "none" });
      keyboard.UpperCaseStatic = false;
      keyboard.upperCase = false;
    }
    gsap.to(keyboard.button[78].key.material, { duration: 0.05, opacity: 0, ease: "none", repeat: 1, yoyo: true });

    
    
    if (!keyboard.upperCase) {
      
      for (let i = 0; i < 76; i++) {
        keyboard.button[i].symbol.geometry.dispose();
        keyboard.button[i].symbol.geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(keyboardData[i], 0.8), 3);
        keyboard.button[i].symbol.geometry.computeBoundingBox();
        keyboard.button[i].symbol.geometry.translate(-keyboard.button[i].symbol.geometry.boundingBox.max.x / 2, -0.4, 0);
        keyboard.button[i].symbol.geometry.NeedUpdate = true;

      }
      gsap.to(keyboard.button[78].symbol2.material, { duration: 0.07, opacity: 0, ease: "none" });

    } else {
      
      for (let i = 0; i < 76; i++) {
        keyboard.button[i].symbol.geometry.dispose();
        keyboard.button[i].symbol.geometry = new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(keyboardData[i].toUpperCase(), 0.8), 3);
        keyboard.button[i].symbol.geometry.computeBoundingBox();
        keyboard.button[i].symbol.geometry.translate(-keyboard.button[i].symbol.geometry.boundingBox.max.x / 2, -0.4, 0);
        keyboard.button[i].symbol.geometry.NeedUpdate = true;
           

      }
    }
    
    
    
    
  }
    intersects = raycaster.intersectObject(keyboard.button[79].key);

    if (intersects.length > 0) {
      gsap.to(keyboard.button[79].key.material, { duration: 0.05, opacity: 0, ease: "power2.out", repeat: 1, yoyo: true });
      goBackSpace();
    }
  }
  
  intersects = raycaster.intersectObject(button.goMovie.plane);
  
  if (intersects.length > 0 && button.goMovie.plane.visible && button.goMovie.border.material.opacity == 1) {
    
    button.goMovie.plane.visible = false;
    onIntro = false;
    startScreenReady = false;
    userInput.ready = false;
    svgPic[0].posY = svgPic[0].position.y;
    stopDrag = false;
    startMovie();
  }
  
  
  
  intersects = raycaster.intersectObject(button.speed.plane);
    if (intersects.length > 0 && button.speed.scale.x == 1) {
      gsap.to([button.speed.icon_1.material, button.speed.icon_2.material], { duration: 0.5, opacity: 0, ease: "none" });
      gsap.to(button.speed.scale, { duration: 2, x: 0, y: 0, ease: "power1.out" });

   
      blurTween.kill();
      blurTween = null;
  
     blurTween = gsap.to(screenBlur, { duration: 3, value: 0.5, ease: "power4.out", onUpdate: function() {
      vblur.uniforms.v.value = screenBlur.value / window.innerHeight * window.devicePixelRatio;
      hblur.uniforms.h.value = screenBlur.value / window.innerWidth * window.devicePixelRatio;
    }, onComplete: function() {
      water.position.y = 1;
      goFin();
    } });
      let positionAttribute = [];
      for (let k = 0; k < text.userText[0].newLine.count; k++) {
        positionAttribute[k] = text.userText[0].newLine[k].geometry.getAttribute('position');
        const vertex = [];
        for (let i = 0; i < positionAttribute[k].count; i += 3) {
         // const  timeDelay = Math.random() * 2;
          const num = i
          let vertex = [new THREE.Vector3(), new THREE.Vector3(), new THREE.Vector3()];
          for (let j = 0; j < 3; j ++) {
            vertex[j].fromBufferAttribute(positionAttribute[k], i + j); // read vertex
          }
          const time = {t: 1};
          if (particleTween[k][i] !== null && particleTween[k][i] !== undefined) {
            particleTween[k][i].kill();
            particleTween[k][i] = null;
          }
          gsap.to(time, { duration: Math.random() * 3, t: 0, ease: "power1.in", onUpdate: function() {
            text.userText[0].newLine[k].geometry.attributes.position.setXYZ(num, vertex[2].x + (vertex[0].x - vertex[2].x) * time.t, vertex[2].y + (vertex[0].y - vertex[2].y) * time.t, vertex[0].z); // write coordinates back
            text.userText[0].newLine[k].geometry.attributes.position.setXYZ(num + 1, vertex[2].x + (vertex[1].x - vertex[2].x) * time.t, vertex[2].y + (vertex[1].y - vertex[2].y) * time.t, vertex[1].z); // write coordinates back
            text.userText[0].newLine[k].geometry.attributes.position.needsUpdate = true;
          } });
        }
      }
      
      
      
      
      let positionAttribute1 = [];
      for (let k = 0; k < text.userText[1].newLine.count; k++) {
        positionAttribute1[k] = text.userText[1].newLine[k].geometry.getAttribute('position');
        const vertex = [];
        for (let i = 0; i < positionAttribute1[k].count; i += 3) {
         // const  timeDelay = Math.random() * 2;
          const num = i
          let vertex = [new THREE.Vector3(), new THREE.Vector3(), new THREE.Vector3()];
          for (let j = 0; j < 3; j ++) {
            vertex[j].fromBufferAttribute(positionAttribute1[k], i + j); // read vertex
          }
          const time = {t: 1};
          if (particleTween1[k][i] !== null && particleTween1[k][i] !== undefined) {
            particleTween1[k][i].kill();
            particleTween1[k][i] = null;
          }
          gsap.to(time, { duration: Math.random() * 3, t: 0, ease: "power1.in", onUpdate: function() {
            text.userText[1].newLine[k].geometry.attributes.position.setXYZ(num, vertex[2].x + (vertex[0].x - vertex[2].x) * time.t, vertex[2].y + (vertex[0].y - vertex[2].y) * time.t, vertex[0].z); // write coordinates back
            text.userText[1].newLine[k].geometry.attributes.position.setXYZ(num + 1, vertex[2].x + (vertex[1].x - vertex[2].x) * time.t, vertex[2].y + (vertex[1].y - vertex[2].y) * time.t, vertex[1].z); // write coordinates back
            text.userText[1].newLine[k].geometry.attributes.position.needsUpdate = true;
          } });
        }
      }
        
     
     
  
  
    
}
  setTimeout(function() {
    requestAnimationFrame(function() {
      touchReady = true;
    });
  }, 100);
}


function fallbackCopyTextToClipboard(text) {
  var textArea = document.createElement("textarea");
  textArea.value = text;
  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand("copy");
    var msg = successful ? "successful" : "unsuccessful";
    console.log("Fallback: Copying text command was " + msg);
  } catch (err) {
    console.error("Fallback: Oops, unable to copy", err);
  }

  document.body.removeChild(textArea);
}
function copy(text) {
  if (!navigator.clipboard) {
    fallbackCopyTextToClipboard(text);
    
    return;
    }
    navigator.clipboard.writeText(text).then(
      function() {
        console.log("Async: Copying to clipboard was successful!");
      },
      function(err) {
        console.error("Async: Could not copy text: ", err);
      }
    );
    }


function onDocumentMouseMove(event) {
  event.preventDefault();
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
  raycaster.setFromCamera(mouse, mainCamera);
  document.body.style.cursor = "default";
  intersects = raycaster.intersectObject(icon.scroll.part[3]);
  if (intersects.length > 0 && icon.scroll.scale.x == 1 && (onFin || onIntro)) {
    document.body.style.cursor = "pointer";
  }
  if (!isMobile()) {
    intersects = raycaster.intersectObject(svgPic[0].plane);
    if (intersects.length > 0) {
      document.body.style.cursor = "pointer";
    }
    intersects = raycaster.intersectObject(svgPic[3].plane);
    if (intersects.length > 0) {
      document.body.style.cursor = "pointer";
    }
    intersects = raycaster.intersectObject(button.product_1.plane);
    if (intersects.length > 0) {
      document.body.style.cursor = "pointer";
    }
    intersects = raycaster.intersectObject(button.product_2.plane);
    if (intersects.length > 0) {
      document.body.style.cursor = "pointer";
      if (!button.product_2.onHover) {
        button.product_2.onHover = true;
      if (button.product_2.hover !== null && button.product_2.hover !== undefined) {
        button.product_2.hover.kill();
        button.product_2.hover = null;
        button.product_2.hoverText.kill();
        button.product_2.hoverText = null;
      }
      button.product_2.hover = gsap.to(button.product_2.plane.material, { duration: 0.3, opacity: 1, ease: "none" });
      button.product_2.hoverText = gsap.to(button.product_2.text.material.color, { duration: 0.3, r: whiteColor.r, g: whiteColor.g, b: whiteColor.b, ease: "none" });

      }
    }
    intersects = raycaster.intersectObject(button.product_3.plane);
    if (intersects.length > 0) {
      document.body.style.cursor = "pointer";
      if (!button.product_3.onHover) {
          button.product_3.onHover = true;
        
      if (button.product_3.hover !== null && button.product_3.hover !== undefined) {
        button.product_3.hover.kill();
        button.product_3.hover = null;
        button.product_3.hoverText.kill();
        button.product_3.hoverText = null;
      }
      button.product_3.hover = gsap.to(button.product_3.plane.material, { duration: 0.3, opacity: 1, ease: "none" });
      button.product_3.hoverText = gsap.to(button.product_3.text.material.color, { duration: 0.3, r: whiteColor.r, g: whiteColor.g, b: whiteColor.b, ease: "none" });

      }
    }
    intersects = raycaster.intersectObject(button.footer_1.plane);
    if (intersects.length > 0) {
      document.body.style.cursor = "pointer";  
    }
  }
  intersects = raycaster.intersectObject(button.popUp.plane);
  if (intersects.length > 0 && button.popUp.ready) {
    document.body.style.cursor = "pointer";  
    if (!button.popUp.onHover) {
      button.popUp.onHover = true;
    
      if (button.popUp.hover !== null && button.sound.hover !== undefined) {
        button.popUp.hover.kill();
        button.popUp.hover = null;
        button.popUp.hoverText.kill();
        button.popUp.hoverText = null;
      }
      button.popUp.hover = gsap.to(button.popUp.plane.material, { duration: 0.3, opacity: 1, ease: "none" });
      button.popUp.hoverText = gsap.to(button.popUp.text.material.color, { duration: 0.3, r: blackColor.r, g: blackColor.g, b: blackColor.b, ease: "none" });
    
    }
  }
  intersects = raycaster.intersectObject(button.sound.plane);
  if (intersects.length > 0 && pic[6].opacity > 0) {
    document.body.style.cursor = "pointer";  
    
  }
  intersects = raycaster.intersectObject(text.footer[0].plane);
  if (intersects.length > 0) {
    document.body.style.cursor = "pointer";    
  }
  intersects = raycaster.intersectObject(text.footer[1].plane);
  if (intersects.length > 0) {
    document.body.style.cursor = "pointer";    
  }
  for (let i = 0; i < 5; i++) {
    intersects = raycaster.intersectObject(productContainer.productPic[i].pic);
    if (intersects.length > 0 && productScrollReady && productContainer.products.position.x != productContainer.productPic[i].position.x) {
      document.body.style.cursor = "pointer";  
    }
  }
  intersects = raycaster.intersectObject(userInput.field[0]);
  if (intersects.length > 0 && userInput.ready && userInput.field[0].visible) {
    document.body.style.cursor = "pointer";  
  }
  intersects = raycaster.intersectObject(userInput.field[1]);
  if (intersects.length > 0 && userInput.ready && userInput.field[1].visible) {
    document.body.style.cursor = "pointer";  
  }
  intersects = raycaster.intersectObject(productContainer.touch);
  if (intersects.length > 0 && finScreenContainer.position.y <= 14) {
    document.body.style.cursor = "pointer";  
  }
 /* intersects = raycaster.intersectObject(text.finText[3].plane);
  if (intersects.length > 0) {
    document.body.style.cursor = "pointer";  
  }*/
  intersects = raycaster.intersectObject(button.goMovie.plane);
  if (intersects.length > 0 && button.goMovie.plane.visible) {
    document.body.style.cursor = "pointer";  
    
    if (!button.goMovie.onHover) {
      button.goMovie.onHover = true;
    if (button.goMovie.hover !== null && button.goMovie.hover !== undefined) {
      button.goMovie.hover.kill();
      button.goMovie.hover = null;
      button.goMovie.hoverText.kill();
      button.goMovie.hoverText = null;
    }
    button.goMovie.hover = gsap.to(button.goMovie.plane.material, { duration: 0.3, opacity: 1, ease: "none" });
    button.goMovie.hoverText = gsap.to(button.goMovie.text.material.color, { duration: 0.3, r: whiteColor.r, g: whiteColor.g, b: whiteColor.b, ease: "none" });


    }
  }
  intersects = raycaster.intersectObject(button.speed.plane);
  if (intersects.length > 0 && button.speed.scale.x == 1) {
    document.body.style.cursor = "pointer";  
  }
  
  if (document.body.style.cursor == "default") {
    if (button.goMovie.onHover) {
      button.goMovie.onHover = false;
      if (button.goMovie.hover !== null && button.goMovie.hover !== undefined) {
        button.goMovie.hover.kill();
        button.goMovie.hover = null;
        button.goMovie.hoverText.kill();
        button.goMovie.hoverText = null;
      }
      button.goMovie.hover = gsap.to(button.goMovie.plane.material, { duration: 0.3, opacity: 0, ease: "none" });
      button.goMovie.hoverText = gsap.to(button.goMovie.text.material.color, { duration: 0.3, r: blackColor.r, g: blackColor.g, b: blackColor.b, ease: "none" });

    }
  }
  if (button.product_2.onHover) {
    button.product_2.onHover = false;
    if (button.product_2.hover !== null && button.product_2.hover !== undefined) {
      button.product_2.hover.kill();
      button.product_2.hover = null;
      button.product_2.hoverText.kill();
      button.product_2.hoverText = null;
    }
    button.product_2.hover = gsap.to(button.product_2.plane.material, { duration: 0.3, opacity: 0, ease: "none" });
    button.product_2.hoverText = gsap.to(button.product_2.text.material.color, { duration: 0.3, r: blackColor.r, g: blackColor.g, b: blackColor.b, ease: "none" });

  }
  if (button.product_3.onHover) {
    button.product_3.onHover = false;
    if (button.product_3.hover !== null && button.product_3.hover !== undefined) {
      button.product_3.hover.kill();
      button.product_3.hover = null;
      button.product_3.hoverText.kill();
      button.product_3.hoverText = null;
    }
    button.product_3.hover = gsap.to(button.product_3.plane.material, { duration: 0.3, opacity: 0, ease: "none" });
    button.product_3.hoverText = gsap.to(button.product_3.text.material.color, { duration: 0.3, r: blackColor.r, g: blackColor.g, b: blackColor.b, ease: "none" });

 
  }
  if (button.popUp.onHover) {
    button.popUp.onHover = false;
    if (button.popUp.hover !== null && button.popUp.hover !== undefined) {
      button.popUp.hover.kill();
      button.popUp.hover = null;
      button.popUp.hoverText.kill();
      button.popUp.hoverText = null;
    }
    button.popUp.hover = gsap.to(button.popUp.plane.material, { duration: 0.3, opacity: 0, ease: "none" });
    button.popUp.hoverText = gsap.to(button.popUp.text.material.color, { duration: 0.3, r: whiteColor.r, g: whiteColor.g, b: whiteColor.b, ease: "none" });

  }
}
